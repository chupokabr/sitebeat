<?php

namespace Sitebeat\Tools\NoindexBundle\Form\Type;

use Sitebeat\ScannerBundle\Form\Type\ToolSettingsFormType;
use Sitebeat\Tools\NoindexBundle\Model\DataTransfer\NoindexSettingsData;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NoindexSettingsType extends AbstractType
{
    public function getParent()
    {
        return ToolSettingsFormType::class;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => NoindexSettingsData::class,
                'method' => 'PUT',
            ]
        );
    }
}
