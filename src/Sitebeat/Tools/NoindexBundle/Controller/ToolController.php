<?php

namespace Sitebeat\Tools\NoindexBundle\Controller;

use AppBundle\Controller\Group\Project\AbstractToolController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sitebeat\ScannerBundle\Entity\Rule;
use Sitebeat\Tools\NoindexBundle\Form\Type\NoindexSettingsType;
use Sitebeat\Tools\NoindexBundle\Model\DataTransfer\NoindexSettingsData;
use Symfony\Component\Form\FormInterface;

/**
 * @Route(service="sitebeat.controller.tool.noindex")
 * @ParamConverter("group", options={"id" = "group_id"})
 * @ParamConverter("project", options={"id" = "project_id"})
 */
class ToolController extends AbstractToolController
{
    protected function createFormDTO(Rule $rule)
    {
        return NoindexSettingsData::fromRuleModel($rule);
    }

    protected static function getFormTypeClassname(): string
    {
        return NoindexSettingsType::class;
    }

    protected static function getFormUpdateTemplateName(): string
    {
        return '@SitebeatToolsNoindex/tool/form_update.html.twig';
    }

    protected static function getToolAlias(): string
    {
        return 'noindex';
    }

    protected function handleFormData(FormInterface $form, Rule $rule): bool
    {
        /** @var NoindexSettingsData $data */
        $data = $form->getData();

        $entityManager = $this->get('doctrine.orm.entity_manager');
        $entityManager->beginTransaction();

        try {
            $rule->setEnabled($data->isEnabled());
            $rule->setRunInterval($data->getRunInterval());
            $entityManager->persist($rule);

            $entityManager->flush();
            $entityManager->commit();
        } catch (\Exception $exception) {
            $entityManager->rollback();

            return false;
        }

        return true;
    }


}
