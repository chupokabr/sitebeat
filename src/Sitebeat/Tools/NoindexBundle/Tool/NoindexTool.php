<?php

namespace Sitebeat\Tools\NoindexBundle\Tool;

use GuzzleHttp\Psr7\Uri;
use League\Uri\Modifiers\HostToAscii;
use League\Uri\Modifiers\KsortQuery;
use League\Uri\Modifiers\Pipeline;
use League\Uri\Modifiers\RemoveDotSegments;
use League\Uri\Schemes\Http;
use Psr\Http\Message\UriInterface;
use Sitebeat\ScannerBundle\Entity\Report;
use Sitebeat\ScannerBundle\ResponseProvider\ResponseProviderInterface;
use Sitebeat\ScannerBundle\Tool\AbstractTool;

class NoindexTool extends AbstractTool
{
    const INCOMPLETE_TAGS_FOUND = 'incomplete_tags_found';
    private $responseProvider;

    /**
     * DomainAccessTool constructor.
     *
     * @param ResponseProviderInterface $responseProvider
     */
    public function __construct(ResponseProviderInterface $responseProvider)
    {
        $this->responseProvider = $responseProvider;
    }

    public function getAlias(): string
    {
        return 'noindex';
    }

    protected function execute()
    {
        $uri = $this->prepareUri($this->rule->getProjectUri());

        $response = $this->responseProvider->request($uri);

        $pageContent = $response->getBody()->getContents();

        $openingTagsCount = preg_match_all('/<(?:!--\s?)?noindex(?:\s?--)?>/im', $pageContent);
        $closingTagsCount = preg_match_all('/<(?:!--\s?)?\/\s?noindex(?:\s?--)?>/im', $pageContent);

        if (in_array($openingTagsCount, [0, false], true)) {
            return;
        }

        $this->reportBuilder->addDetail(
            'found',
            [
                'opened' => $openingTagsCount,
                'closed' => $closingTagsCount,
            ]
        );

        if ($openingTagsCount !== $closingTagsCount) {
            $this->reportBuilder->setStatus(static::INCOMPLETE_TAGS_FOUND);
        }
    }

    protected function getStatusLevels(): array
    {
        return [
            static::INCOMPLETE_TAGS_FOUND => Report::DANGER,
        ];
    }

    /**
     * @param string $uri
     *
     * @return UriInterface
     */
    protected function prepareUri(string $uri): UriInterface
    {
        $modifier = (new Pipeline())
            ->pipe(new RemoveDotSegments())
            ->pipe(new HostToAscii())
            ->pipe(new KsortQuery());

        $modifiedUri = (string) $modifier->process(Http::createFromString($uri));

        return new Uri($modifiedUri);
    }

}
