<?php

namespace Sitebeat\Tools\PageAccessBundle\Tool;

use GuzzleHttp\Psr7\Uri;
use League\Uri\Modifiers\HostToAscii;
use League\Uri\Modifiers\KsortQuery;
use League\Uri\Modifiers\Pipeline;
use League\Uri\Modifiers\RemoveDotSegments;
use League\Uri\Schemes\Http;
use Psr\Http\Message\UriInterface;
use Sitebeat\ScannerBundle\Entity\Report;
use Sitebeat\ScannerBundle\ResponseProvider\ResponseProviderInterface;
use Sitebeat\ScannerBundle\Tool\AbstractTool;
use Sitebeat\Tools\PageAccessBundle\Entity\PageRepository;
use Sitebeat\Tools\PageAccessBundle\Exception\NotFoundPageException;
use Sitebeat\Tools\PageAccessBundle\Model\Page;

/**
 * Class PageAccessTool
 *
 * @package Sitebeat\Tools\PageAccess
 */
class PageAccessTool extends AbstractTool
{
    const ALIAS = 'page_access';

    const SERVER_ERROR = 'server_error';
    const CLIENT_ERROR = 'client_error';
    const PAGE_REDIRECT = 'page_redirect';
    /**
     * @var ResponseProviderInterface
     */
    private $responseProvider;
    /**
     * @var PageRepository
     */
    private $pageRepository;

    /**
     * DomainAccessTool constructor.
     *
     * @param ResponseProviderInterface $responseProvider
     * @param PageRepository            $pageRepository
     */
    public function __construct(ResponseProviderInterface $responseProvider, PageRepository $pageRepository)
    {
        $this->responseProvider = $responseProvider;
        $this->pageRepository = $pageRepository;
    }

    /**
     * @return string
     */
    public function getAlias(): string
    {
        return static::ALIAS;
    }

    /**
     * @return bool
     */
    protected function execute()
    {
        //find tracked pages
        $pages = $this->getPages();

        //process pages

        foreach ($pages as $page) {
            $this->processPage($page);
        }

        return true;
    }

    /**
     * @param Page $page
     */
    protected function processPage(Page $page)
    {
        $pageData = [];
        $pageData['status'] = 'success';

        //prepare uri to request
        $uri = $this->prepareUri($this->rule->getProjectUri(), $page);

        //get response
        $response = $this->responseProvider->request($uri, ['allow_redirects' => false]);

        // resolve status code
        $statusCode = $response->getStatusCode();

        if ($page->getExpectedStatusCode() !== $statusCode) {
            $pageData['status'] = 'warning';

            switch (true) {
                case 500 <= $statusCode:
                    $this->reportBuilder->setStatus(static::SERVER_ERROR);
                    $pageData['status'] = 'danger';
                    break;
                case 400 <= $statusCode:
                    $this->reportBuilder->setStatus(static::CLIENT_ERROR);
                    $pageData['status'] = 'danger';
                    break;
                case 300 <= $statusCode:
                    $this->reportBuilder->setStatus(static::PAGE_REDIRECT);
                    break;
            }
        }

        //set essential report details
        $pageData['uri'] = (string) $uri;
        $pageData['response'] = $statusCode.': '.$response->getReasonPhrase();
        $pageData['expected_code'] = $page->getExpectedStatusCode();

        if ($response->hasHeader('Location')) {
            $pageData['response'] .= ' ('.$response->getHeaderLine('Location').')';
        }

        $this->reportBuilder->addDetail('pages', $pageData);
    }

    /**
     * @return Page[]
     */
    protected function getPages()
    {
        $pages = $this->pageRepository->findBy(['rule' => $this->rule]);

        if (0 === count($pages)) {
            throw new NotFoundPageException('There are no pages found for rule.');
        }

        return $pages;
    }

    /**
     * @param string $uri
     *
     * @return UriInterface
     */
    protected function prepareUri(string $uri, Page $page): UriInterface
    {
        $modifier = (new Pipeline())
            ->pipe(new RemoveDotSegments())
            ->pipe(new HostToAscii())
            ->pipe(new KsortQuery());

        $modifiedUri = (string) $modifier->process(Http::createFromString($uri)->withPath($page->getPath()));

        return new Uri($modifiedUri);
    }

    /**
     * @return array
     */
    protected function getStatusLevels(): array
    {
        return [
            static::SERVER_ERROR => Report::DANGER,
            static::CLIENT_ERROR => Report::DANGER,
            static::PAGE_REDIRECT => Report::WARNING,
        ];
    }
}
