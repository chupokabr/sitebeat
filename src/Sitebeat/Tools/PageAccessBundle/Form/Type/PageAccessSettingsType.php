<?php

namespace Sitebeat\Tools\PageAccessBundle\Form\Type;

use Sitebeat\ScannerBundle\Form\Type\ToolSettingsFormType;
use Sitebeat\Tools\PageAccessBundle\Model\DataTransfer\PageAccessSettingsData;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PageAccessSettingsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'pages',
                CollectionType::class,
                [
                    'entry_type' => PageType::class,
                    'label' => false,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false,
                    'delete_empty' => true,
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => PageAccessSettingsData::class,
                'translation_domain' => 'page_access_settings',
                'method' => 'PUT',
            ]
        );
    }


    /**
     * @return string
     */
    public function getParent()
    {
        return ToolSettingsFormType::class;
    }
}
