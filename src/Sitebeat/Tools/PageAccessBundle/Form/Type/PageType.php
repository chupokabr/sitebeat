<?php

namespace Sitebeat\Tools\PageAccessBundle\Form\Type;

use Sitebeat\Tools\PageAccessBundle\Model\DataTransfer\PageData;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PageType extends AbstractType
{
    protected $rule;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('path', TextType::class, ['required' => true])
            ->add('code', IntegerType::class, ['required' => true]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults(
                [
                    'data_class' => PageData::class,
                ]
            );
    }
}
