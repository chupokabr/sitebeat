<?php

namespace Sitebeat\Tools\PageAccessBundle\Exception;

use Sitebeat\ScannerBundle\Exception\ExceptionInterface;

class NotFoundPageException extends \RuntimeException implements ExceptionInterface
{
}
