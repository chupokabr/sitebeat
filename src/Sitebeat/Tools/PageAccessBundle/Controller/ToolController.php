<?php

namespace Sitebeat\Tools\PageAccessBundle\Controller;

use AppBundle\Controller\Group\Project\AbstractToolController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sitebeat\ScannerBundle\Entity\Rule;
use Sitebeat\Tools\PageAccessBundle\Entity\Page;
use Sitebeat\Tools\PageAccessBundle\Form\Type\PageAccessSettingsType;
use Sitebeat\Tools\PageAccessBundle\Model\DataTransfer\PageAccessSettingsData;
use Sitebeat\Tools\PageAccessBundle\Model\DataTransfer\PageData;
use Symfony\Component\Form\FormInterface;

/**
 * @Route(service="sitebeat.controller.tool.page_access")
 * @ParamConverter("group", options={"id" = "group_id"})
 * @ParamConverter("project", options={"id" = "project_id"})
 */
class ToolController extends AbstractToolController
{
    protected static function getFormTypeClassname(): string
    {
        return PageAccessSettingsType::class;
    }

    protected static function getFormUpdateTemplateName(): string
    {
        return '@SitebeatToolsPageAccess/tool/form_update.html.twig';
    }

    protected static function getToolAlias(): string
    {
        return 'page_access';
    }

    protected function createFormDTO(Rule $rule)
    {
        $data = PageAccessSettingsData::fromRuleModel($rule);

        $pages = $this->getPages($rule);

        if (null !== $pages) {
            foreach ($pages as $page) {
                $data->addPage(PageData::fromPageModel($page));
            }
        }

        return $data;
    }

    protected function handleFormData(FormInterface $form, Rule $rule): bool
    {
        /** @var PageAccessSettingsData $data */
        $data = $form->getData();

        $entityManager = $this->get('doctrine.orm.entity_manager');
        $entityManager->beginTransaction();

        try {
            $rule->setEnabled($data->isEnabled());
            $rule->setRunInterval($data->getRunInterval());
            $entityManager->persist($rule);


            // delete existing pages
            $pages = $this->getPages($rule);

            if (null !== $pages) {
                foreach ($pages as $page) {
                    $entityManager->remove($page);
                }
            }

            //persist new pages
            $pagesData = $data->getPages();

            foreach ($pagesData as $pageData) {
                $entityManager->persist(new Page($rule, $pageData->getPath(), $pageData->getCode()));
            }

            $entityManager->flush();
            $entityManager->commit();
        } catch (\Exception $exception) {
            $entityManager->rollback();

            return false;
        }

        return true;
    }

    /**
     * @param Rule $rule
     *
     * @return Page[]|null
     */
    protected function getPages(Rule $rule): array
    {
        return $this->get('sitebeat.repository.page')->findBy(['rule' => $rule]);
    }
}
