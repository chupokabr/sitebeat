<?php

namespace Sitebeat\Tools\PageAccessBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Sitebeat\Tools\PageAccessBundle\Model\Page as BasePage;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="PageRepository")
 * @ORM\Table(name="pages")
 */
class Page extends BasePage
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;
    /**
     * @ORM\Column(type="string")
     */
    protected $path;
    /**
     * @ORM\Column(type="smallint")
     */
    protected $expectedStatusCode;
    /**
     * @ORM\ManyToOne(targetEntity="Sitebeat\ScannerBundle\Entity\Rule")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $rule;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
}
