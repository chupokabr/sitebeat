<?php

namespace Sitebeat\Tools\PageAccessBundle\Model\DataTransfer;

use Sitebeat\Tools\PageAccessBundle\Model\Page;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class PageData
 *
 * @package Sitebeat\Tools\PageAccessBundle\Model\DataTrasfer
 */
class PageData
{
    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Assert\Regex("/^\/(\w+)?\/?/i")
     */
    private $path = '';
    /**
     * @var int
     *
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Assert\Range(min="100", max="511")
     */
    private $code = 200;

    /**
     * @param Page $page
     *
     * @return static
     */
    public static function fromPageModel(Page $page)
    {
        $data = new static();
        $data->setCode($page->getExpectedStatusCode());
        $data->setPath($page->getPath());

        return $data;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath(string $path)
    {
        $this->path = $path;
    }

    /**
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param int $code
     */
    public function setCode(int $code)
    {
        $this->code = $code;
    }
}
