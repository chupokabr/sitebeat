<?php

namespace Sitebeat\Tools\PageAccessBundle\Model\DataTransfer;

use AppBundle\Model\DataTransfer\AbstractToolSettingsData;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class PageAccessSettingsData
 *
 * @package Sitebeat\Tools\PageAccessBundle\Model
 */
class PageAccessSettingsData extends AbstractToolSettingsData
{
    /**
     * @var array
     *
     * @Assert\Valid()
     */
    private $pages;

    public function __construct()
    {
        $this->pages = new ArrayCollection();
    }

    /**
     * @return PageData[]
     */
    public function getPages()
    {
        return $this->pages;
    }

    /**
     * @param PageData[] $pages
     */
    public function setPages(array $pages)
    {
        foreach ($pages as $page) {
            $this->addPage($page);
        }
    }

    /**
     * @param PageData $page
     */
    public function addPage(PageData $page)
    {
        $this->pages->add($page);
    }

    /**
     * @param PageData $page
     */
    public function removePage(PageData $page)
    {
        $this->pages->removeElement($page);
    }
}
