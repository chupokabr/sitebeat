<?php

namespace Sitebeat\Tools\PageAccessBundle\Model;

use Sitebeat\ScannerBundle\Model\Rule;

/**
 * Class Page
 *
 * @package Sitebeat\Tools\PageAccessBundle\Model
 */
class Page
{
    /**
     * @var string
     */
    protected $path;
    /**
     * @var int
     */
    protected $expectedStatusCode;
    /**
     * @var Rule
     */
    protected $rule;

    /**
     * Page constructor.
     *
     * @param Rule   $rule
     * @param string $uri
     * @param int    $expectedStatusCode
     */
    public function __construct(Rule $rule, string $uri, int $expectedStatusCode = 200)
    {
        $this->path = $uri;
        $this->expectedStatusCode = $expectedStatusCode;
        $this->rule = $rule;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @return int
     */
    public function getExpectedStatusCode(): int
    {
        return $this->expectedStatusCode;
    }

    /**
     * @param string $path
     */
    public function setPath(string $path)
    {
        $this->path = $path;
    }

    /**
     * @param int $expectedStatusCode
     */
    public function setExpectedStatusCode(int $expectedStatusCode)
    {
        $this->expectedStatusCode = $expectedStatusCode;
    }
}
