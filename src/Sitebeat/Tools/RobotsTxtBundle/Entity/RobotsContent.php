<?php

namespace Sitebeat\Tools\RobotsTxtBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Sitebeat\ScannerBundle\Entity\Rule;

/**
 * @ORM\Entity(repositoryClass="RobotsContentRepository")
 * @ORM\Table(name="robots_contents")
 */
class RobotsContent
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="Sitebeat\ScannerBundle\Entity\Rule")
     */
    private $rule;
    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $content;

    /**
     * RobotsContent constructor.
     *
     * @param Rule   $rule
     * @param string $content
     */
    public function __construct(Rule $rule, string $content)
    {
        $this->content = $content;
        $this->rule = $rule;
    }
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @return Rule
     */
    public function getRule()
    {
        return $this->rule;
    }

    /**
     * @param string $content
     */
    public function updateContent(string $content)
    {
        $this->content = $content;
    }
}
