<?php

namespace Sitebeat\Tools\RobotsTxtBundle\Tool;

use Diff;
use GuzzleHttp\Psr7\Uri;
use League\Uri\Modifiers\HostToAscii;
use League\Uri\Modifiers\KsortQuery;
use League\Uri\Modifiers\Pipeline;
use League\Uri\Modifiers\RemoveDotSegments;
use League\Uri\Schemes\Http;
use Psr\Http\Message\UriInterface;
use Sitebeat\ScannerBundle\Entity\Report;
use Sitebeat\ScannerBundle\ResponseProvider\ResponseProviderInterface;
use Sitebeat\ScannerBundle\Tool\AbstractTool;
use Sitebeat\Tools\RobotsTxtBundle\Entity\RobotsContent;
use Sitebeat\Tools\RobotsTxtBundle\Entity\RobotsContentRepository;

class RobotsTxtTool extends AbstractTool
{
    const HAS_SYNTAX_ERRORS = 'has_syntax_errors';
    const FILE_NOT_FOUND = 'file_not_found';
    const FILE_IS_EMPTY = 'file_is_empty';
    const NOT_IDENTICAL_WITH_SAMPLE = 'not_identical_with_sample';
    /**
     * @var ResponseProviderInterface
     */
    private $responseProvider;
    /**
     * @var RobotsContentRepository
     */
    private $contentRepository;


    public function __construct(ResponseProviderInterface $responseProvider, RobotsContentRepository $contentRepository)
    {
        $this->responseProvider = $responseProvider;
        $this->contentRepository = $contentRepository;
    }

    /**
     * @param $robotsContent
     *
     * @return array|string[]
     */
    private static function findSyntaxErrors($robotsContent)
    {
        $syntaxErrors = [];

        if (!preg_match('/User-agent:/im', $robotsContent)) {
            $syntaxErrors[] = 'Отсутствуют директивы юзер-агентов';
        }

        if (preg_match('/User-agent:[\s\/]*$/im', $robotsContent)) {
            $syntaxErrors[] = 'Инструкция User-agent имеет неверные символы';
        }

        if (preg_match('/Disallow: ?[\w\/\.\*]+? +\S+[\w\s]*$/im', $robotsContent)) {
            $syntaxErrors[] = 'Инструкция Disallow содержит несколько адресов';
        }

        if (preg_match('/Host: http[s]?:/im', $robotsContent)) {
            $syntaxErrors[] = 'Инструкция Host содержит \'http\'';
        }

        return $syntaxErrors;
    }

    public function getAlias(): string
    {
        return 'robots_txt';
    }

    protected function execute()
    {
        $uri = $this->prepareUri($this->rule->getProjectUri());

        $response = $this->responseProvider->request($uri->withPath('/robots.txt'));

        // проверка на наличие
        if ($response->getStatusCode() >= 400) {
            $this->reportBuilder->setStatus(static::FILE_NOT_FOUND);
            $this->reportBuilder->addDetail('requested_url', (string) $uri->withPath('/robots.txt'));

            return null;
        }

        $robotsContent = preg_replace('~\R~u', "\r\n", trim($response->getBody()->getContents()));
        // сравнение с эталоном
        $sample = preg_replace('~\R~u', "\r\n", $this->getRobotsSample()->getContent());
        if (null !== $sample) {
            $diff = $this->compare($robotsContent, $sample);

            if (null !== $diff) {
                $this->reportBuilder->setStatus(static::NOT_IDENTICAL_WITH_SAMPLE);
                $this->reportBuilder->addDetail('diff', $diff);

                return null;
            }
        }

        // проверяем, не пустой ли файл
        if ('' === $robotsContent) {
            $this->reportBuilder->setStatus(static::FILE_IS_EMPTY);
            $this->reportBuilder->addDetail('requested_url', (string) $uri->withPath('/robots.txt'));

            return null;
        }

        //проверяем файл на синтаксические ошибки
        $syntaxErrors = self::findSyntaxErrors($robotsContent);

        if ([] !== $syntaxErrors) {
            $this->reportBuilder->setStatus(static::HAS_SYNTAX_ERRORS);
            foreach ($syntaxErrors as $syntaxError) {
                $this->reportBuilder->addDetail('syntax_errors', $syntaxError);
            }

            return null;
        }

        $this->reportBuilder->addDetail('content', nl2br($robotsContent));
    }

    protected function getStatusLevels(): array
    {
        return [
            self::HAS_SYNTAX_ERRORS => Report::DANGER,
            self::FILE_NOT_FOUND => Report::DANGER,
            self::FILE_IS_EMPTY => Report::DANGER,
            self::NOT_IDENTICAL_WITH_SAMPLE => Report::DANGER,
        ];
    }

    /**
     * @param string $uri
     *
     * @return UriInterface
     */
    protected function prepareUri(string $uri): UriInterface
    {
        $modifier = (new Pipeline())
            ->pipe(new RemoveDotSegments())
            ->pipe(new HostToAscii())
            ->pipe(new KsortQuery());

        $modifiedUri = (string) $modifier->process(Http::createFromString($uri));

        return new Uri($modifiedUri);
    }

    /**
     * @param $robotsContent
     *
     * @return string|null
     */
    private function compare(string $robotsContent, string $sample)
    {
        if ($sample === $robotsContent) {
            return null;
        }

        $diff = new Diff(explode(PHP_EOL, $sample), explode(PHP_EOL, $robotsContent));

        return $diff->render(new \Diff_Renderer_Html_SideBySide());

    }

    /**
     * @return RobotsContent|null
     */
    private function getRobotsSample()
    {
        return $this->contentRepository->findOneBy(['rule' => $this->rule]);
    }
}