<?php

namespace Sitebeat\Tools\RobotsTxtBundle\Controller;

use AppBundle\Controller\Group\Project\AbstractToolController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sitebeat\ScannerBundle\Entity\Rule;
use Sitebeat\Tools\RobotsTxtBundle\Entity\RobotsContent;
use Sitebeat\Tools\RobotsTxtBundle\Form\Type\RobotsTxtSettingsType;
use Sitebeat\Tools\RobotsTxtBundle\Model\DataTransfer\RobotsTxtSettingsData;
use Symfony\Component\Form\FormInterface;

/**
 * @Route(service="sitebeat.controller.tool.robots_txt")
 * @ParamConverter("group", options={"id" = "group_id"})
 * @ParamConverter("project", options={"id" = "project_id"})
 */
class ToolController extends AbstractToolController
{
    protected static function getToolAlias(): string
    {
        return 'robots_txt';
    }

    protected static function getFormTypeClassname(): string
    {
        return RobotsTxtSettingsType::class;
    }

    protected static function getFormUpdateTemplateName(): string
    {
        return '@SitebeatToolsRobotsTxt/tool/form_update.html.twig';
    }

    protected function createFormDTO(Rule $rule)
    {
        $data = RobotsTxtSettingsData::fromRuleModel($rule);

        $robotsContent = $this
            ->getDoctrine()
            ->getRepository('SitebeatToolsRobotsTxtBundle:RobotsContent')
            ->findOneBy(['rule' => $rule]);

        $data->setContent($robotsContent->getContent());

        return $data;
    }

    protected function handleFormData(FormInterface $form, Rule $rule): bool
    {
        /** @var RobotsTxtSettingsData $data */
        $data = $form->getData();

        $entityManager = $this->get('doctrine.orm.entity_manager');
        $entityManager->beginTransaction();

        try {
            $rule->setEnabled($data->isEnabled());
            $rule->setRunInterval($data->getRunInterval());
            $entityManager->persist($rule);

            /** @var RobotsContent $content */
            $content = $entityManager->getRepository('SitebeatToolsRobotsTxtBundle:RobotsContent')
                ->findOneBy(['rule' => $rule]);

            $content->updateContent($data->getContent());

            $entityManager->persist($content);

            $entityManager->flush();
            $entityManager->commit();
        } catch (\Exception $exception) {
            $entityManager->rollback();

            return false;
        }

        return true;
    }
}
