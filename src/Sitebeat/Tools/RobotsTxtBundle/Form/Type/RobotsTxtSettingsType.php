<?php

namespace Sitebeat\Tools\RobotsTxtBundle\Form\Type;

use Sitebeat\ScannerBundle\Form\Type\ToolSettingsFormType;
use Sitebeat\Tools\RobotsTxtBundle\Model\DataTransfer\RobotsTxtSettingsData;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RobotsTxtSettingsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'content',
                TextareaType::class,
                [
                    'required' => false,
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => RobotsTxtSettingsData::class,
                'translation_domain' => 'robots_txt_settings',
                'method' => 'PUT',
            ]
        );
    }

    public function getParent()
    {
        return ToolSettingsFormType::class;
    }
}
