<?php

namespace Sitebeat\Tools\RobotsTxtBundle\Model\DataTransfer;

use AppBundle\Model\DataTransfer\AbstractToolSettingsData;

/**
 * Class RobotsTxtSettingsData
 *
 * @package Sitebeat\Tools\RobotsTxtBundle\Model\DataTransfer
 */
class RobotsTxtSettingsData extends AbstractToolSettingsData
{
    /**
     * @var string
     */
    private $content;

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content)
    {
        $this->content = $content;
    }
}
