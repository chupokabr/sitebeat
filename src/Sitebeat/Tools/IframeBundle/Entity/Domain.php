<?php

namespace Sitebeat\Tools\IframeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Sitebeat\ScannerBundle\Entity\Rule;

/**
 * Class Domain
 *
 * @package Sitebeat\Tools\IframeBundle\Entity
 * @ORM\Entity(repositoryClass="DomainRepository")
 * @ORM\Table(name="domains")
 */
class Domain
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $content;
    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $enabled;
    /**
     * @ORM\ManyToOne(targetEntity="Sitebeat\ScannerBundle\Entity\Rule")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $rule;

    /**
     * Domain constructor.
     *
     * @param Rule   $rule
     * @param string $content
     * @param bool   $enabled
     */
    public function __construct($rule, $content, $enabled = true)
    {
        $this->content = $content;
        $this->enabled = $enabled;
        $this->rule = $rule;
    }

    /**
     * @return Rule
     */
    public function getRule()
    {
        return $this->rule;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->enabled;
    }
}
