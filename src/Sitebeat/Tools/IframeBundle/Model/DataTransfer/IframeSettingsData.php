<?php

namespace Sitebeat\Tools\IframeBundle\Model\DataTransfer;

use AppBundle\Model\DataTransfer\AbstractToolSettingsData;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

class IframeSettingsData extends AbstractToolSettingsData
{
    /**
     * @var array
     *
     * @Assert\Valid()
     */
    private $domains;

    public function __construct()
    {
        $this->domains = new ArrayCollection();
    }
    
    /**
     * @return DomainData[]
     */
    public function getDomains()
    {
        return $this->domains;
    }

    /**
     * @param DomainData[] $domains
     */
    public function setDomains(array $domains)
    {
        foreach ($domains as $domain) {
            $this->addDomain($domain);
        }
    }

    /**
     * @param DomainData $domain
     */
    public function addDomain(DomainData $domain)
    {
        $this->domains->add($domain);
    }

    /**
     * @param DomainData $domain
     */
    public function removeDomain(DomainData $domain)
    {
        $this->domains->removeElement($domain);
    }
}
