<?php

namespace Sitebeat\Tools\IframeBundle\Model\DataTransfer;

use Sitebeat\Tools\IframeBundle\Entity\Domain;
use Symfony\Component\Validator\Constraints as Assert;

class DomainData
{
    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $content = '';
    /**
     * @var bool
     */
    private $enabled = false;

    /**
     * @param Domain $domain
     *
     * @return static
     */
    public static function fromDomainModel(Domain $domain)
    {
        $data = new static();
        $data->setContent($domain->getContent());
        $data->setEnabled($domain->isEnabled());

        return $data;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content)
    {
        $this->content = $content;
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param int $enabled
     */
    public function setEnabled(bool $enabled)
    {
        $this->enabled = $enabled;
    }


}
