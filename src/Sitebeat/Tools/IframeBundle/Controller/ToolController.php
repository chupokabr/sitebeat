<?php

namespace Sitebeat\Tools\IframeBundle\Controller;

use AppBundle\Controller\Group\Project\AbstractToolController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sitebeat\ScannerBundle\Entity\Rule;
use Sitebeat\Tools\IframeBundle\Entity\Domain;
use Sitebeat\Tools\IframeBundle\Form\Type\IframeSettingsType;
use Sitebeat\Tools\IframeBundle\Model\DataTransfer\DomainData;
use Sitebeat\Tools\IframeBundle\Model\DataTransfer\IframeSettingsData;
use Symfony\Component\Form\FormInterface;

/**
 * @Route(service="sitebeat.controller.tool.iframe")
 * @ParamConverter("group", options={"id" = "group_id"})
 * @ParamConverter("project", options={"id" = "project_id"})
 */
class ToolController extends AbstractToolController
{
    protected static function getToolAlias(): string
    {
        return 'iframe';
    }

    protected static function getFormTypeClassname(): string
    {
        return IframeSettingsType::class;
    }

    protected static function getFormUpdateTemplateName(): string
    {
        return '@SitebeatToolsIframe/tool/form_update.html.twig';
    }

    protected function createFormDTO(Rule $rule)
    {
        $data = IframeSettingsData::fromRuleModel($rule);

        $domains = $this->getDomains($rule);

        if (null !== $domains) {
            foreach ($domains as $domain) {
                $data->addDomain(DomainData::fromDomainModel($domain));
            }
        }

        return $data;
    }

    protected function handleFormData(FormInterface $form, Rule $rule): bool
    {
        /** @var IframeSettingsData $data */
        $data = $form->getData();

        $entityManager = $this->get('doctrine.orm.entity_manager');
        $entityManager->beginTransaction();

        try {
            $rule->setEnabled($data->isEnabled());
            $rule->setRunInterval($data->getRunInterval());
            $entityManager->persist($rule);


            // delete existing domains
            $domains = $this->getDomains($rule);

            if (null !== $domains) {
                foreach ($domains as $domain) {
                    $entityManager->remove($domain);
                }
            }

            //persist new domains
            $domainsData = $data->getDomains();

            foreach ($domainsData as $domainData) {
                $entityManager->persist(new Domain($rule, $domainData->getContent(), $domainData->isEnabled()));
            }

            $entityManager->flush();
            $entityManager->commit();
        } catch (\Exception $exception) {
            $entityManager->rollback();

            return false;
        }

        return true;
    }

    /**
     * @param $rule
     *
     * @return Domain[]|null
     */
    protected function getDomains($rule): array
    {
        return $this->get('sitebeat.repository.domain')->findBy(['rule' => $rule]);
    }

}
