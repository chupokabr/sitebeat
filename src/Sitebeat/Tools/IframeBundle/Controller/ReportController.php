<?php

namespace Sitebeat\Tools\IframeBundle\Controller;

use AppBundle\Controller\Group\ReportControllerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sitebeat\ScannerBundle\Entity\Report;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route(service="sitebeat.controller.report.iframe")
 * @ParamConverter("report", options={"id" = "report_id"})
 */
class ReportController extends Controller implements ReportControllerInterface
{
    /**
     * @Method("GET")
     */
    public function showAction(Report $report)
    {
        return $this->render('@SitebeatToolsIframe/report/show.html.twig', ['report' => $report]);
    }
}
