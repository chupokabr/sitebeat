<?php

namespace Sitebeat\Tools\IframeBundle\Tool;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use GuzzleHttp\Psr7\Uri;
use League\Uri\Modifiers\HostToAscii;
use League\Uri\Modifiers\KsortQuery;
use League\Uri\Modifiers\Pipeline;
use League\Uri\Modifiers\RemoveDotSegments;
use League\Uri\Schemes\Http;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\UriInterface;
use Sitebeat\ScannerBundle\Entity\Report;
use Sitebeat\ScannerBundle\ResponseProvider\GuzzleResponseProvider;
use Sitebeat\ScannerBundle\Tool\AbstractTool;
use Sitebeat\Tools\IframeBundle\Entity\Domain;
use Sitebeat\Tools\IframeBundle\Entity\DomainRepository;
use Symfony\Component\DomCrawler\Crawler;

class IframeTool extends AbstractTool
{
    const UNEXPECTED_IFRAMES_FOUND = 'unexpected_iframes_found';
    const NO_IFRAME_FOUND = 'no_iframe_found';
    /**
     * @var DomainRepository
     */
    private $domainRepository;
    /**
     * @var GuzzleResponseProvider
     */
    private $responseProvider;

    public function __construct(GuzzleResponseProvider $responseProvider, DomainRepository $domainRepository)
    {
        $this->responseProvider = $responseProvider;
        $this->domainRepository = $domainRepository;
    }

    public function getAlias(): string
    {
        return 'iframe';
    }

    protected function execute()
    {
        $uri = $this->prepareUri($this->rule->getProjectUri());

        $response = $this->responseProvider->request($uri);

        // получить ссылки iframe на странице
        $iframeUrls = $this->extractIframesFromResponse($response);

        // если ссылок нет, вернуть

        if ($iframeUrls->isEmpty()) {
            $this->reportBuilder->setStatus(static::NO_IFRAME_FOUND);

            return;
        }


        // отфильтровать ссылки, не совпадающие со взятыми из репа.
        /** @var ArrayCollection|Domain[] $allowedDomains */
        $allowedDomains = $this->domainRepository->findBy(['rule' => $this->rule]);

        /** @var ArrayCollection|UriInterface[] $unexpectedUris */
        $unexpectedUris = $iframeUrls->filter(
            function (UriInterface $uri) use ($allowedDomains) {
                foreach ($allowedDomains as $domain) {
                    if (false === stripos((string) $uri->getHost(), $domain->getContent())) {
                        return true;
                    }
                }
            }
        );

        foreach ($iframeUrls as $uri) {
            $this->reportBuilder->addDetail(
                'iframes',
                [
                    'content' => (string) $uri,
                    'enabled' => (bool) !$unexpectedUris->contains($uri),
                ]
            );
        }

        // если неразрешенные iframe есть, то поставить статус
        if (!$unexpectedUris->isEmpty()) {
            $this->reportBuilder->setStatus(static::UNEXPECTED_IFRAMES_FOUND);
        }
    }

    protected function prepareUri(string $uri): UriInterface
    {
        $modifier = (new Pipeline())
            ->pipe(new RemoveDotSegments())
            ->pipe(new HostToAscii())
            ->pipe(new KsortQuery());

        $modifiedUri = (string) $modifier->process(Http::createFromString($uri));

        return new Uri($modifiedUri);
    }

    protected function getStatusLevels(): array
    {
        return [
            static::UNEXPECTED_IFRAMES_FOUND => Report::DANGER,
            static::NO_IFRAME_FOUND => Report::SUCCESS,
        ];
    }

    /**
     * @param ResponseInterface $response
     *
     * @return Collection|UriInterface[]
     * @throws \RuntimeException
     */
    protected function extractIframesFromResponse(ResponseInterface $response)
    {
        $iframes = new ArrayCollection();

        $responseContents = $response->getBody()->getContents();

        (new Crawler($responseContents))->filter('iframe')->each(
            function (Crawler $node) use ($iframes) {
                $iframes->add($this->prepareUri($node->extract(['src'])[0]));
            }
        );

        return $iframes;
    }
}
