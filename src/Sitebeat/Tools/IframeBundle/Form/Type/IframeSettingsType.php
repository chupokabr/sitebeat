<?php

namespace Sitebeat\Tools\IframeBundle\Form\Type;

use Sitebeat\ScannerBundle\Form\Type\ToolSettingsFormType;

use Sitebeat\Tools\IframeBundle\Model\DataTransfer\IframeSettingsData;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class IframeSettingsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'domains',
                CollectionType::class,
                [
                    'entry_type' => DomainType::class,
                    'label' => false,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false,
                    'delete_empty' => true,
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => IframeSettingsData::class,
                'translation_domain' => 'iframe_settings',
                'method' => 'PUT',
            ]
        );
    }

    public function getParent()
    {
        return ToolSettingsFormType::class;
    }

}
