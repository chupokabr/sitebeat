<?php

namespace Sitebeat\Tools\DomainAccessBundle\Form;

use Sitebeat\ScannerBundle\Form\Type\ToolSettingsFormType;
use Sitebeat\Tools\DomainAccessBundle\Model\DataTransfer\DomainAccessSettingsData;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DomainAccessSettingsType extends AbstractType
{
    public function getParent()
    {
        return ToolSettingsFormType::class;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => DomainAccessSettingsData::class,
                'method' => 'PUT',
            ]
        );
    }
}
