<?php

namespace Sitebeat\Tools\DomainAccessBundle\Controller;

use AppBundle\Controller\Group\Project\AbstractToolController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sitebeat\ScannerBundle\Entity\Rule;
use Sitebeat\Tools\DomainAccessBundle\Form\DomainAccessSettingsType;
use Sitebeat\Tools\DomainAccessBundle\Model\DataTransfer\DomainAccessSettingsData;
use Symfony\Component\Form\FormInterface;

/**
 * @Route(service="sitebeat.controller.tool.domain_access")
 * @ParamConverter("group", options={"id" = "group_id"})
 * @ParamConverter("project", options={"id" = "project_id"})
 */
class ToolController extends AbstractToolController
{
    protected static function getFormTypeClassname(): string
    {
        return DomainAccessSettingsType::class;
    }

    protected static function getFormUpdateTemplateName(): string
    {
        return '@SitebeatToolsDomainAccess/tool/form_update.html.twig';
    }

    protected static function getToolAlias(): string
    {
        return 'domain_access';
    }

    protected function createFormDTO(Rule $rule)
    {
        return DomainAccessSettingsData::fromRuleModel($rule);
    }

    protected function handleFormData(FormInterface $form, Rule $rule): bool
    {
        /** @var DomainAccessSettingsData $data */
        $data = $form->getData();

        $entityManager = $this->get('doctrine.orm.entity_manager');
        $entityManager->beginTransaction();

        try {
            $rule->setEnabled($data->isEnabled());
            $rule->setRunInterval($data->getRunInterval());
            $entityManager->persist($rule);

            $entityManager->flush();
            $entityManager->commit();
        } catch (\Exception $exception) {
            $entityManager->rollback();

            return false;
        }

        return true;
    }
}
