<?php

namespace Sitebeat\Tools\DomainAccessBundle\Controller;

use AppBundle\Controller\Group\ReportControllerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sitebeat\ScannerBundle\Entity\Report;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route(service="sitebeat.controller.report.domain_access")
 * @ParamConverter("report", options={"id" = "report_id"})
 */
class ReportController extends Controller implements ReportControllerInterface
{
    /**
     * @Method("GET")
     */
    public function showAction(Report $report)
    {
        return $this->render('@SitebeatToolsDomainAccess/report/show.html.twig', ['report' => $report]);
    }
}
