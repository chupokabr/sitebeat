<?php

namespace Sitebeat\Tools\DomainAccessBundle\Tool;

use GuzzleHttp\Psr7\Uri;
use League\Uri\Modifiers\HostToAscii;
use League\Uri\Modifiers\KsortQuery;
use League\Uri\Modifiers\Pipeline;
use League\Uri\Modifiers\RemoveDotSegments;
use League\Uri\Schemes\Http;
use Psr\Http\Message\UriInterface;
use Sitebeat\ScannerBundle\Entity\Report;
use Sitebeat\ScannerBundle\ResponseProvider\ResponseProviderInterface;
use Sitebeat\ScannerBundle\Tool\AbstractTool;

/**
 * Class DomainAccessTool
 *
 * @package Sitebeat\Tools\DomainAccess
 */
class DomainAccessTool extends AbstractTool
{
    const ALIAS = 'domain_access';

    const SERVER_ERROR = 'server_error';
    const CLIENT_ERROR = 'client_error';
    const PAGE_REDIRECT = 'page_redirect';
    /**
     * @var ResponseProviderInterface
     */
    private $responseProvider;

    /**
     * DomainAccessTool constructor.
     *
     * @param ResponseProviderInterface $responseProvider
     */
    public function __construct(ResponseProviderInterface $responseProvider)
    {
        $this->responseProvider = $responseProvider;
    }

    /**
     * @return string
     */
    public function getAlias(): string
    {
        return static::ALIAS;
    }

    /**
     * @return bool
     */
    protected function execute()
    {
        $uri = $this->prepareUri($this->rule->getProjectUri());

        $response = $this->responseProvider->request($uri, ['allow_redirects' => false]);

        // resolve status code
        $statusCode = $response->getStatusCode();

        switch (true) {
            case 500 <= $statusCode:
                $this->reportBuilder->setStatus(static::SERVER_ERROR);
                break;
            case 400 <= $statusCode:
                $this->reportBuilder->setStatus(static::CLIENT_ERROR);
                break;
            case 300 <= $statusCode:
                $this->reportBuilder->setStatus(static::PAGE_REDIRECT);
                $this->reportBuilder->addDetail('location', $response->getHeaderLine('Location'));
                break;
        }

        $this->reportBuilder->addDetail('status_code', $statusCode);
        $this->reportBuilder->addDetail('reason_phrase', $response->getReasonPhrase());

        return true;
    }

    /**
     * @param string $uri
     *
     * @return UriInterface
     */
    protected function prepareUri(string $uri): UriInterface
    {
        $modifier = (new Pipeline())
            ->pipe(new RemoveDotSegments())
            ->pipe(new HostToAscii())
            ->pipe(new KsortQuery());

        $modifiedUri = (string) $modifier->process(Http::createFromString($uri));

        return new Uri($modifiedUri);
    }

    /**
     * @return array
     */
    protected function getStatusLevels(): array
    {
        return [
            static::SERVER_ERROR => Report::DANGER,
            static::CLIENT_ERROR => Report::DANGER,
            static::PAGE_REDIRECT => Report::WARNING,
        ];
    }
}
