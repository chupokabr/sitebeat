<?php

namespace Sitebeat\NotifierBundle\EventListener;

use AppBundle\Entity\AccountRepository;
use Sitebeat\NotifierBundle\Mailer\NotifierMailer;
use Sitebeat\ScannerBundle\Event\ReportEvent;
use Symfony\Component\Templating\EngineInterface;

/**
 * Class SendReportListener
 *
 * @package Sitebeat\NotifierBundle\EventListener
 */
class SendSingleReportMailListener
{
    /**
     * @var NotifierMailer
     */
    private $mailer;
    /**
     * @var AccountRepository
     */
    private $accountRepository;
    /**
     * @var EngineInterface
     */
    private $templating;

    /**
     * SendReportListener constructor.
     *
     * @param \Swift_Mailer     $mailer
     * @param AccountRepository $accountRepository
     * @param EngineInterface   $templating
     */
    public function __construct(
        \Swift_Mailer $mailer,
        AccountRepository $accountRepository,
        EngineInterface $templating
    ) {
        $this->mailer = $mailer;
        $this->accountRepository = $accountRepository;
        $this->templating = $templating;
    }

    /**
     * @param ReportEvent $event
     */
    public function onReportCreateFinish(ReportEvent $event)
    {
        $report = $event->getReport();

        $subscribers = $this->accountRepository->findAllSubscribedToProject($report->getProject());

        foreach ($subscribers as $account) {
            $renderedReport = $this->templating->render(
                '@SitebeatNotifier/single_report.html.twig',
                ['report' => $report]
            );

            $message = \Swift_Message::newInstance()
                ->setTo($account->getEmail())
                ->setFrom('robot@sitebeat.kelnik.ru')
                ->setContentType('text/html')
                ->setSubject('Результат сканирования '.$report->getProjectUri())
                ->setBody($renderedReport);

            $this->mailer->send($message);
        }
    }
}
