<?php

namespace Sitebeat\NotifierBundle\Mailer;

use FOS\UserBundle\Doctrine\UserManager;
use Sitebeat\ScannerBundle\Entity\Report;
use Sitebeat\ScannerBundle\Entity\ReportRepository;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;

class NotifierMailer
{
    /**
     * @var EngineInterface
     */
    private $templating;
    /**
     * @var \Swift_Mailer
     */
    private $mailer;
    /**
     * @var ReportRepository
     */
    private $reportRepository;
    /**
     * @var UserManager
     */
    private $userManager;

    public function __construct(
        EngineInterface $templating,
        \Swift_Mailer $mailer,
        ReportRepository $reportRepository,
        UserManager $userManager
    ) {
        $this->templating = $templating;
        $this->mailer = $mailer;
        $this->reportRepository = $reportRepository;
        $this->userManager = $userManager;
    }

    public function sendReport(Report $report)
    {
        // ...
    }
}
