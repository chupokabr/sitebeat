<?php

namespace Sitebeat\NotifierBundle\Command;

use Sitebeat\ScannerBundle\Entity\Report;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SendHourlyReportsCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('sitebeat:report:hourly')
            ->setDescription('Send hourly reports to all users');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $templating = $container->get('templating');
        $reportsRepository = $container->get('sitebeat.repository.report');
        $mailer = $container->get('mailer');

        $reports = $reportsRepository->findAllDangerUnviewed();

        $reportsStorage = [];

        foreach ($reports as $report) {
            $email = $report->getAssignee()->getEmail();

            $reportsStorage[$email][] = $report;
        }

        if (0 === count($reportsStorage)) {
            return;
        }

        /** @var Report[] $reports */
        foreach ($reportsStorage as $email => $reports) {
            echo $email."\n";
            $body = $templating->render(
                '@SitebeatNotifier/hourly_report.html.twig',
                ['reports' => $reports]
            );

            $message = \Swift_Message::newInstance()
                ->setTo($email)
                ->setFrom('robot@sitebeat.kelnik.ru')
                ->setContentType('text/html')
                ->setSubject('Непрочитанные критические уведомления')
                ->setBody($body);

            $mailer->send($message);
        }
    }
}
