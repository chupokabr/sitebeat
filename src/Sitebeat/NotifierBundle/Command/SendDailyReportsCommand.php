<?php

namespace Sitebeat\NotifierBundle\Command;

use AppBundle\Entity\Project;
use Sitebeat\ScannerBundle\Entity\Report;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SendDailyReportsCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('sitebeat:report:daily')
            ->setDescription('Send daily reports to all users.');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $templating = $container->get('templating');
        $projectRepository = $container->get('app.repository.project');
        $mailer = $container->get('mailer');

        /** @var Project[] $projects */
        $projects = $projectRepository->findAll();

        $emailStorage = [];

        foreach ($projects as $project) {
            $subscribers = $project->getSubscribers();
            $reports = $project->getLatestReports();

            foreach ($subscribers as $subscriber) {
                foreach ($reports as $report) {
                    $emailStorage[$subscriber->getEmail()][] = $report;
                }
            }
        }

        if (0 === count($emailStorage)) {
            return;
        }

        /** @var Report[] $reports */
        foreach ($emailStorage as $email => $reports) {
            echo $email."\n";
            $body = $templating->render(
                '@SitebeatNotifier/hourly_report.html.twig',
                ['reports' => $reports]
            );

            $message = \Swift_Message::newInstance()
                ->setTo($email)
                ->setFrom('robot@sitebeat.kelnik.ru')
                ->setContentType('text/html')
                ->setSubject('Полный отчет по всем проектам')
                ->setBody($body);

            $mailer->send($message);
        }
    }
}
