<?php

namespace Sitebeat\ScannerBundle\ResponseProvider;

use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use Kevinrob\GuzzleCache\CacheMiddleware;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\UriInterface;

/**
 * Class GuzzleResponseProvider
 *
 * @package Sitebeat\ScannerBundle\ResponseProvider
 */
class GuzzleResponseProvider implements ResponseProviderInterface
{
    private $client;

    /**
     * ResponseProvider constructor.
     */
    public function __construct()
    {
        $stack = HandlerStack::create();
        $stack->push(new CacheMiddleware(), 'cache');

        $this->client = new Client(
            [
                'http_errors' => false,
                'handler' => $stack,
            ]
        );
    }

    /**
     * @param UriInterface|string $uri
     * @param array               $options
     *
     * @return ResponseInterface
     */
    public function request($uri, array $options = []): ResponseInterface
    {
        return $this->client->request('GET', $uri, $options);
    }
}
