<?php

namespace Sitebeat\ScannerBundle\ResponseProvider;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\UriInterface;

/**
 * Interface ResponseProviderInterface
 *
 * @package Sitebeat\ScannerBundle\ResponseProvider
 */
interface ResponseProviderInterface
{
    /**
     * @param UriInterface|string $uri
     * @param array               $options
     *
     * @return ResponseInterface
     */
    public function request($uri, array $options = []): ResponseInterface;
}
