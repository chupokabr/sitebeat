<?php

namespace Sitebeat\ScannerBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RunIntervalType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'choices' => [
                    'run_interval.options.every_15_mins' => 900,
                    'run_interval.options.every_1_hour' => 3600,
                    'run_interval.options.every_4_hours' => 14400,
                    'run_interval.options.every_24_hours' => 86400,
                    'run_interval.options.every_1_week' => 604800,
                ],
                'translation_domain' => 'tool_settings',
            ]
        );
    }


    public function getParent()
    {
        return ChoiceType::class;
    }
}
