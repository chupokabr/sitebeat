<?php

namespace Sitebeat\ScannerBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class ToolPass
 *
 * @package Sitebeat\ScannerBundle\DependencyInjection\Compiler
 */
class ToolPass implements CompilerPassInterface
{
    /**
     * @param ContainerBuilder $container
     *
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\InvalidArgumentException
     */
    public function process(ContainerBuilder $container)
    {
        $toolboxDefinition = 'sitebeat.scanner.rule_processor';
        $toolTag = 'sitebeat.tool';

        if (!$container->has($toolboxDefinition)) {
            return;
        }

        $definition = $container->getDefinition($toolboxDefinition);
        $taggedServices = $container->findTaggedServiceIds($toolTag);

        /** @var \Traversable $tags */
        foreach ($taggedServices as $id => $tags) {
            foreach ($tags as $attributes) {
                $definition->addMethodCall('addTool', [new Reference($id), $attributes['alias']]);
            }
        }
    }
}
