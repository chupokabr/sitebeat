<?php

namespace Sitebeat\ScannerBundle\Exception;

class NotFoundToolException extends \InvalidArgumentException implements ExceptionInterface
{
}
