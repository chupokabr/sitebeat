<?php

namespace Sitebeat\ScannerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Sitebeat\ScannerBundle\Model\Rule as BaseRule;

/**
 * @ORM\Entity(repositoryClass="RuleRepository")
 * @ORM\Table(name="rules")
 */
class Rule extends BaseRule
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;
    /**
     * @ORM\Column(type="string")
     */
    protected $toolAlias;

    /**
     * @ORM\Column(type="integer")
     */
    protected $runInterval;
    /**
     * @ORM\Column(type="datetime")
     */
    protected $readyToRunAt;
    /**
     * @ORM\Column(type="boolean")
     */
    protected $enabled;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Project", inversedBy="rules")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $project;
    /**
     * @ORM\OneToMany(targetEntity="Sitebeat\ScannerBundle\Entity\Report",mappedBy="rule")
     */
    protected $reports;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $forced;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
}
