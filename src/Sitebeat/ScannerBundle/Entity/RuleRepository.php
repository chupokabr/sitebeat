<?php

namespace Sitebeat\ScannerBundle\Entity;

use AppBundle\Entity\Project;
use Doctrine\ORM\EntityRepository;

/**
 * RuleRepository
 *
 * This class was generated by the PhpStorm "Php Annotations" Plugin. Add your own custom
 * repository methods below.
 */
class RuleRepository extends EntityRepository
{
    /**
     * @return Rule[]|null
     */
    public function findAllReadyToRun()
    {
        $qb = $this->createQueryBuilder('rule');

        $qb->where(
            $qb->expr()->andX(
                $qb->expr()->eq('rule.enabled', true),
                $qb->expr()->lt('rule.readyToRunAt', ':now')
            )
        )->setParameter('now', new \DateTime());

        return $qb
            ->getQuery()
            ->execute();
    }

    public function findOneByProjectAndTool(Project $project, string $toolAlias)
    {
        $qb = $this->createQueryBuilder('rule');

        return $qb
            ->where('rule.toolAlias = :tool_alias AND rule.project = :project')
            ->join('rule.project', 'project')
            ->addSelect('project')
            ->setParameters(
                [
                    'project' => $project,
                    'tool_alias' => $toolAlias,
                ]
            )
            ->getQuery()
            ->getOneOrNullResult();
    }
}
