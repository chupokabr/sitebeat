<?php

namespace Sitebeat\ScannerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Sitebeat\ScannerBundle\Model\Report as BaseReport;
use Sitebeat\ScannerBundle\Model\Rule;

/**
 * @ORM\Entity(repositoryClass="ReportRepository")
 * @ORM\Table(name="reports")
 */
class Report extends BaseReport
{
    /**
     * @var \Ramsey\Uuid\Uuid
     *
     * @ORM\Id
     * @ORM\Column(type="uuid")
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     */
    protected $id;
    /**
     * @ORM\ManyToOne(targetEntity="Sitebeat\ScannerBundle\Entity\Rule", inversedBy="reports")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $rule;
    /**
     * @ORM\Column(type="smallint")
     */
    protected $statusLevel;
    /**
     * @ORM\Column(type="string")
     */
    protected $header;
    /**
     * @ORM\Column(type="array")
     */
    protected $details;
    /**
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $viewed;

    public function __construct(
        Rule $rule,
        $statusLevel,
        $header,
        array $details,
        \DateTimeInterface $createdAt = null,
        $viewed = false
    ) {
        parent::__construct($rule, $statusLevel, $header, $details, $createdAt, $viewed);
        $this->id = Uuid::uuid4();
    }


    /**
     * @return UuidInterface
     */
    public function getId()
    {
        return $this->id;
    }
}
