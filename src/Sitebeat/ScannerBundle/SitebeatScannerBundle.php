<?php

namespace Sitebeat\ScannerBundle;

use Sitebeat\ScannerBundle\DependencyInjection\Compiler\ToolPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class SitebeatScannerBundle
 */
class SitebeatScannerBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new ToolPass());
    }
}
