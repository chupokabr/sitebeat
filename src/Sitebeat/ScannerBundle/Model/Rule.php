<?php

namespace Sitebeat\ScannerBundle\Model;

/**
 * Class Rule
 *
 * @package Sitebeat\ScannerBundle\Model
 */
use AppBundle\Model\Account;
use AppBundle\Model\Project;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class Rule
 *
 * @package Sitebeat\ScannerBundle\Model
 */
class Rule
{
    /**
     * @var string
     */
    protected $toolAlias;
    /**
     * Maximum run interval in minutes.
     *
     * @var int
     */
    protected $runInterval;
    /**
     * @var \DateTimeInterface
     */
    protected $readyToRunAt;
    /**
     * @var bool
     */
    protected $enabled;
    /**
     * @var Project
     */
    protected $project;
    /**
     * @var Report[]
     */
    protected $reports;
    /**
     * @var bool
     */
    protected $forced;

    /**
     * Rule constructor.
     *
     * @param Project            $project
     * @param string             $toolAlias
     * @param int                $runInterval
     * @param \DateTimeInterface $readyToRunAt
     * @param bool               $enabled
     * @param bool               $forced
     */
    public function __construct(
        Project $project,
        string $toolAlias,
        int $runInterval,
        \DateTimeInterface $readyToRunAt = null,
        bool $enabled = false,
        bool $forced = false
    ) {
        $this->reports = new ArrayCollection();
        $this->toolAlias = $toolAlias;
        $this->runInterval = $runInterval;
        $this->readyToRunAt = $readyToRunAt ?? new \DateTime();
        $this->enabled = $enabled;
        $this->project = $project;
        $this->forced = $forced;
    }

    /**
     * @param Report $report
     */
    public function addReport(Report $report)
    {
        $this->reports[] = $report;
    }

    /**
     * @return Report
     */
    public function getLastReport(): Report
    {
        $report = $this->reports->last();
        $this->reports->first();

        return $report;
    }

    /**
     * @return bool
     */
    public function hasReports(): bool
    {
        return !$this->reports->isEmpty();
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     */
    public function setEnabled(bool $enabled)
    {
        $this->enabled = $enabled;
    }

    /**
     * @return string
     */
    public function getToolAlias(): string
    {
        return $this->toolAlias;
    }

    /**
     * @return int
     */
    public function getRunInterval(): int
    {
        return $this->runInterval;
    }

    /**
     * Set run interval in minutes.
     *
     * @param int $runInterval
     */
    public function setRunInterval(int $runInterval)
    {
        if (1 > $runInterval) {
            throw new \InvalidArgumentException();
        }

        $this->runInterval = $runInterval;
    }

    /**
     * Defer rule processing to next run.
     */
    public function deferRun()
    {
        $this->readyToRunAt = $this->roundUpTime(new \DateTime(), $this->runInterval);
        $this->forced = false;
    }

    public function forceToRun()
    {
        $this->readyToRunAt = new \DateTime();
        $this->forced = true;
    }

    /**
     * @return string
     */
    public function getProjectUri()
    {
        return $this->project->getUri();
    }

    /**
     * @return Account
     */
    public function getProjectAssignee()
    {
        return $this->project->getAssignee();
    }

    /**
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @return bool
     */
    public function isForced()
    {
        return $this->forced;
    }

    /**
     * @param \DateTime $dateTime
     * @param int       $period
     *
     * @return \DateTime
     */
    protected function roundUpTime(\DateTime $dateTime, int $period)
    {
        $leftover = $dateTime->getTimestamp() % $period;

        if ($leftover > 0) {
            $leftover = $period - $leftover;
        }

        return $dateTime->add(\DateInterval::createFromDateString($leftover.' seconds'));
    }
}
