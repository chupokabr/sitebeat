<?php

namespace Sitebeat\ScannerBundle\Model;

use AppBundle\Model\Account;

/**
 * Class Report
 *
 * @package Sitebeat\ScannerBundle\Model
 */
class Report
{
    const DANGER = 3;
    const WARNING = 2;
    const SUCCESS = 1;

    const STATUS_CODES = [
        self::SUCCESS => 'success',
        self::WARNING => 'warning',
        self::DANGER => 'danger',
    ];

    /**
     * @var Rule
     */
    protected $rule;
    /**
     * @var int
     */
    protected $statusLevel;
    /**
     * @var string
     */
    protected $header;
    /**
     * @var array
     */
    protected $details;
    /**
     * @var \DateTimeInterface
     */
    protected $createdAt;

    /**
     * @var bool
     */
    protected $viewed;

    /**
     * Report constructor.
     *
     * @param Rule               $rule
     * @param int                $statusLevel
     * @param string             $header
     * @param array              $details
     * @param \DateTimeInterface $createdAt
     * @param bool               $viewed
     */
    public function __construct(
        Rule $rule,
        int $statusLevel,
        string $header,
        array $details,
        \DateTimeInterface $createdAt = null,
        bool $viewed = false
    ) {
        $this->rule = $rule;
        $this->statusLevel = $statusLevel;
        $this->details = $details;
        $this->header = $header;
        $this->createdAt = $createdAt ?? new \DateTime();
        $this->viewed = $viewed;
    }

    /**
     * @return string
     */
    public function getToolAlias()
    {
        return $this->rule->getToolAlias();
    }

    /**
     * @return int
     */
    public function getStatusLevel(): int
    {
        return $this->statusLevel;
    }

    /**
     * @return string
     */
    public function getHeader(): string
    {
        return $this->header;
    }

    /**
     * @return array
     */
    public function getDetails(): array
    {
        return $this->details;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getCreatedAt(): \DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @return string
     */
    public function getProjectUri(): string
    {
        return $this->rule->getProjectUri();
    }

    /**
     * @return Account
     */
    public function getAssignee(): Account
    {
        return $this->rule->getProjectAssignee();
    }

    /**
     * @return bool
     */
    public function isViewed(): bool
    {
        return $this->viewed;
    }

    public function markAsViewed()
    {
        $this->viewed = true;
    }

    /**
     * @return \AppBundle\Model\Project
     */
    public function getProject()
    {
        return $this->rule->getProject();
    }
}
