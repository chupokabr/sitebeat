<?php

namespace Sitebeat\ScannerBundle;

class ScannerEvents
{
    const REPORT_CREATE_FINISH = 'report.create.finish';
}
