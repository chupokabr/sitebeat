<?php

namespace Sitebeat\ScannerBundle\Tool;

use Sitebeat\ScannerBundle\Model\Report;
use Sitebeat\ScannerBundle\Model\Rule;
use Sitebeat\ScannerBundle\Service\ReportBuilder;

/**
 * Class AbstractTool
 *
 * @package Sitebeat\Scanner
 */
abstract class AbstractTool
{
    const UNKNOWN_ERROR = 'unknown_error';

    /**
     * @var Rule
     */
    protected $rule;
    /**
     * @var ReportBuilder
     */
    protected $reportBuilder;

    /**
     * @param Rule $rule
     *
     * @return Report
     */
    public function process(Rule $rule): Report
    {
        //fool protection
        if ($rule->getToolAlias() !== $this->getAlias()) {
            throw new \InvalidArgumentException();
        }

        $this->rule = $rule;

        $this->reportBuilder = new ReportBuilder($rule);
        $this->reportBuilder->updateStatusLevels($this->getStatusLevels());

        try {
            $this->execute();
        } catch (\Throwable $exception) {
            $this->reportBuilder->setStatus(self::UNKNOWN_ERROR);
            $this->reportBuilder->setException($exception->getMessage());
        }

        $report = $this->reportBuilder->getReport();

        unset($this->reportBuilder);

        return $report;
    }

    /**
     * @return string
     */
    abstract public function getAlias(): string;

    abstract protected function execute();

    public function __toString()
    {
        return $this->getAlias();
    }

    /**
     * @return array
     */
    abstract protected function getStatusLevels(): array;
}
