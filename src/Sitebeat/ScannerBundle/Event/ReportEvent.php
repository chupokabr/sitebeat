<?php

namespace Sitebeat\ScannerBundle\Event;

use Sitebeat\ScannerBundle\Entity\Report;
use Symfony\Component\EventDispatcher\Event;

class ReportEvent extends Event
{
    private $report;

    public function __construct(Report $report)
    {
        $this->report = $report;
    }

    /**
     * @return Report
     */
    public function getReport()
    {
        return $this->report;
    }
}
