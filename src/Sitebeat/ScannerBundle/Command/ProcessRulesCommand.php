<?php

namespace Sitebeat\ScannerBundle\Command;

use Sitebeat\ScannerBundle\Entity\Rule;
use Sitebeat\ScannerBundle\Event\ReportEvent;
use Sitebeat\ScannerBundle\ScannerEvents;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ProcessRulesCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('sitebeat:scanner:process')
            ->setDescription('Process all rules.');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $em = $container->get('doctrine.orm.entity_manager');
        $dispatcher = $container->get('event_dispatcher');
        $ruleProcessor = $container->get('sitebeat.scanner.rule_processor');

        /** @var Rule[] $rules */
        $rules = $em->getRepository('SitebeatScannerBundle:Rule')->findAllReadyToRun();

        foreach ($rules as $rule) {
            $report = $ruleProcessor->process($rule);
            $em->persist($report);

            if ($rule->isForced()) {
                $dispatcher->dispatch(ScannerEvents::REPORT_CREATE_FINISH, new ReportEvent($report));
            }

            $rule->deferRun();
            $em->persist($rule);
        }

        $em->flush();
    }
}
