<?php

namespace Sitebeat\ScannerBundle\Service;

use Doctrine\Common\Proxy\Exception\InvalidArgumentException;
use Sitebeat\ScannerBundle\Entity\Report;
use Sitebeat\ScannerBundle\Model\Rule;

/**
 * Class ReportBuilder
 *
 * @package Sitebeat\Scanner
 */
class ReportBuilder
{
    const SUCCESS = 'success';
    const UNKNOWN_ERROR = 'unknown_error';
    /**
     * @var array
     */
    protected static $statusLevels = [
        self::UNKNOWN_ERROR => Report::DANGER,
        self::SUCCESS => Report::SUCCESS,
    ];
    /**
     * @var Rule
     */
    protected $rule;
    /**
     * @var $details
     */
    protected $details = [];
    /**
     * @var string
     */
    protected $status = 'success';

    protected $statusLevel = Report::SUCCESS;

    /**
     * ReportBuilder constructor.
     *
     * @param Rule $rule
     */
    public function __construct(Rule $rule)
    {
        $this->rule = $rule;
    }

    /**
     * @return Report
     */
    public function getReport(): Report
    {
        $this->statusLevel = $this->resolveStatusLevel($this->status);

        return new Report($this->rule, $this->statusLevel, $this->status, $this->details);
    }

    /**
     * @param string $alias
     * @param mixed  $content
     */
    public function addDetail($alias, $content)
    {
        $this->details[$alias][] = $content;
    }

    /**
     * @param string $message
     */
    public function setException(string $message)
    {
        $this->details = [];
        $this->details['exception'] = $message;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status)
    {
        if ($this->statusLevel < $this->resolveStatusLevel($status)) {
            $this->status = $status;
        }
    }

    /**
     * @param array $statusLevels
     */
    public function updateStatusLevels(array $statusLevels)
    {
        static::$statusLevels = array_merge(static::$statusLevels, $statusLevels);
    }

    /**
     * @param string $status
     *
     * @return int
     */
    protected function resolveStatusLevel(string $status): int
    {
        if (!array_key_exists($status, static::$statusLevels)) {
            throw new InvalidArgumentException('Unknown status level set');
        }

        return static::$statusLevels[$status];
    }
}
