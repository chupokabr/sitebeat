<?php

namespace Sitebeat\ScannerBundle\Service;

use Sitebeat\ScannerBundle\Event\ReportEvent;
use Sitebeat\ScannerBundle\Exception\NotFoundToolException;
use Sitebeat\ScannerBundle\Model\Report;
use Sitebeat\ScannerBundle\Model\Rule;
use Sitebeat\ScannerBundle\ScannerEvents;
use Sitebeat\ScannerBundle\Tool\AbstractTool;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class RuleProcessor
 *
 * @package Sitebeat\Scanner
 */
class RuleProcessor
{
    /**
     * @var AbstractTool[]
     */
    protected $tools = [];
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * RuleProcessor constructor.
     *
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(EventDispatcherInterface $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param AbstractTool $tool
     * @param string       $alias
     *
     * @return self
     */
    public function addTool(AbstractTool $tool, string $alias): self
    {
        $this->tools[$alias] = $tool;

        return $this;
    }

    /**
     * @param Rule $rule
     *
     * @return Report
     */
    public function process(Rule $rule): Report
    {
        $alias = $rule->getToolAlias();

        if (!$this->hasTool($alias)) {
            throw new NotFoundToolException(sprintf('There is no tool with such alias "%s" found.', $alias));
        }

        return $this->tools[$alias]->process($rule);
    }

    /**
     * @return array
     */
    public function getDefinedToolAliases(): array
    {
        return array_keys($this->tools);
    }

    /**
     * @param string $alias
     *
     * @return bool
     */
    protected function hasTool(string $alias): bool
    {
        return array_key_exists($alias, $this->tools);
    }
}
