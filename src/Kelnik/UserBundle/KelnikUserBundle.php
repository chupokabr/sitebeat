<?php

namespace Kelnik\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class KelnikUserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
