<?php

namespace Kelnik\UserBundle\Model;

use FOS\UserBundle\Model\User as BaseUser;

/**
 * Class User
 */
class User extends BaseUser implements UserInterface
{
    /**
     * @var string
     */
    protected $firstName;
    /**
     * @var string
     */
    protected $lastName;

    /**
     * @param string $email
     *
     * @return $this
     */
    public function setEmail($email)
    {
        $this->setUsername($email);
        parent::setEmail($email);

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     *
     * @return $this
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     *
     * @return $this
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }
}
