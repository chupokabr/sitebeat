<?php

namespace Kelnik\UserBundle\DataFixtures\ORM;

use AppBundle\Model\Account;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Nelmio\Alice\Fixtures;

class LoadFixtures implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        Fixtures::load(
            __DIR__.'/fixtures.yml',
            $manager,
            [
                'providers' => [$this],
            ]
        );
    }

    public function role()
    {
        $roles = [
            Account::GROUP_ADMIN,
            Account::GROUP_MANAGER,
            Account::GROUP_GUEST
        ];

        $key = array_rand($roles);

        return $roles[$key];
    }
}
