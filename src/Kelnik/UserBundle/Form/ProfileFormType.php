<?php

namespace Kelnik\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * {@inheritdoc}
 */
class ProfileFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->remove('username')
            ->remove('current_password')
            ->add(
                'firstName',
                null,
                [
                    'required' => false,
                    'label' => 'form.first_name',
                    'translation_domain' => 'FOSUserBundle',
                ]
            )
            ->add(
                'lastName',
                null,
                [
                    'required' => false,
                    'label' => 'form.last_name',
                    'translation_domain' => 'FOSUserBundle',
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'kelnik_user_profile';
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\ProfileFormType';
    }
}
