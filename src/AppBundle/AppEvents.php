<?php

namespace AppBundle;

class AppEvents
{
    const GROUP_CREATE_SUCCESS = 'app.group.create.success';
    const GROUP_CREATE_ERROR = 'app.group.create.error';
    const GROUP_UPDATE_SUCCESS = 'app.group.update.success';
    const GROUP_DELETE_SUCCESS = 'app.group.delete.success';

    const PROJECT_CREATE_COMPLETED = 'app.project.create.completed';
    const PROJECT_UPDATE_COMPLETED = 'app.project.update.completed';
    const PROJECT_CREATE_ERROR = 'app.project.create.error';
    const PROJECT_DELETE_COMPLETED = 'app.project.delete.completed';

    const ACCOUNT_CREATE_COMPLETED = 'app.account.create.completed';
    const ACCOUNT_UPDATE_COMPLETED = 'app.account.update.completed';
    const ACCOUNT_DELETE_COMPLETED = 'app.account.delete.completed';
}
