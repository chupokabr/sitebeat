<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Model\Account as BaseAccount;

/**
 * @ORM\Entity(repositoryClass="AccountRepository")
 * @ORM\Table(name="accounts")
 */
class Account extends BaseAccount
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;
    /**
     * @ORM\ManyToOne(targetEntity="Kelnik\UserBundle\Entity\User")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $user;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Group")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $group;
    /**
     * @ORM\Column(type="string")
     */
    protected $role;
    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Project", mappedBy="subscribers")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $projects;

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
}
