<?php

namespace AppBundle\Entity;

use AppBundle\Model\Project as BaseProject;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Project.
 *
 * @ORM\Entity(repositoryClass="ProjectRepository")
 * @ORM\Table(name="projects")
 */
class Project extends BaseProject
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;
    /**
     * @ORM\Column(type="string")
     */
    protected $uri;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Account")
     */
    protected $assignee;
    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Account", inversedBy="projects")
     * @ORM\JoinTable(name="accounts_projects")
     */
    protected $subscribers;
    /**
     * @ORM\OneToMany(targetEntity="Sitebeat\ScannerBundle\Entity\Rule", mappedBy="project")
     */
    protected $rules;

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
}
