<?php

namespace AppBundle\Twig\Extension;

use Sitebeat\ScannerBundle\Model\Report;

/**
 * Class StatusCodeExtension
 *
 * @package Kelnik\SeoScanBundle\Twig\Extension
 */
class StatusCodeExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return [new \Twig_SimpleFilter('status_code', [$this, 'translateStatusCode'])];
    }

    public function translateStatusCode($statusCode)
    {
        return Report::STATUS_CODES[$statusCode];
    }

    public function getName()
    {
        return 'status_code';
    }
}
