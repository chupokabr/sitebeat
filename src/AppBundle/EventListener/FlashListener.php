<?php

namespace AppBundle\EventListener;

use AppBundle\AppEvents;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Translation\TranslatorInterface;

class FlashListener implements EventSubscriberInterface
{
    private static $successMessages = [
        AppEvents::GROUP_CREATE_SUCCESS => 'group.flash.created',
        AppEvents::GROUP_CREATE_ERROR => 'group.flash.error',
        AppEvents::GROUP_UPDATE_SUCCESS => 'group.flash.updated',
        AppEvents::GROUP_DELETE_SUCCESS => 'group.flash.deleted',

        AppEvents::PROJECT_UPDATE_COMPLETED => 'project.flash.updated',
        AppEvents::PROJECT_CREATE_ERROR => 'project.flash.error',
        AppEvents::PROJECT_CREATE_COMPLETED => 'project.flash.created',
        AppEvents::PROJECT_DELETE_COMPLETED => 'project.flash.deleted',
    ];

    private $session;
    private $translator;

    public function __construct(Session $session, TranslatorInterface $translator)
    {
        $this->session = $session;
        $this->translator = $translator;
    }

    public static function getSubscribedEvents()
    {
        return [
            AppEvents::GROUP_CREATE_SUCCESS => 'addSuccessFlash',
            AppEvents::GROUP_CREATE_ERROR => 'addErrorFlash',
            AppEvents::GROUP_UPDATE_SUCCESS => 'addSuccessFlash',
            AppEvents::GROUP_DELETE_SUCCESS => 'addSuccessFlash',

            AppEvents::PROJECT_UPDATE_COMPLETED => 'addSuccessFlash',
            AppEvents::PROJECT_CREATE_ERROR => 'addErrorFlash',
            AppEvents::PROJECT_CREATE_COMPLETED => 'addSuccessFlash',
            AppEvents::PROJECT_DELETE_COMPLETED => 'addSuccessFlash',
        ];
    }

    public function addSuccessFlash(Event $event, $eventName)
    {
        $this->addFlash('success', $eventName);
    }

    public function addErrorFlash(Event $event, $eventName)
    {
        $this->addFlash('danger', $eventName);
    }

    private function addFlash(string $status, string $eventName)
    {
        if (!array_key_exists($eventName, self::$successMessages)) {
            throw new \InvalidArgumentException('This event does not correspond to a known flash message');
        }

        $this->session->getFlashBag()->add($status, $this->trans(self::$successMessages[$eventName]));
    }

    private function trans($message, array $params = [])
    {
        return $this->translator->trans($message, $params, 'messages');
    }
}
