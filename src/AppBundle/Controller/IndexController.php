<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class IndexController
 *
 * @package AppBundle\Controller
 */
class IndexController extends Controller
{
    /**
     * Show user accounts list.
     *
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        return $this->forward('AppBundle:Group:list');
    }

    /**
     * @Route("/test", name="")
     */
    public function testAction()
    {
        return $this->render('SitebeatNotifierBundle::single_report.html.twig');
    }
}
