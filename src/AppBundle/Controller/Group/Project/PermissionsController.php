<?php

namespace AppBundle\Controller\Group\Project;

use AppBundle\AppEvents;
use AppBundle\Entity\Group;
use AppBundle\Entity\Project;
use AppBundle\Form\Type\ProjectPermissionsType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * @Route("/groups/{group_id}/projects/{project_id}/permissions", requirements={"group_id": "\d+", "project_id": "\d+"})
 * @ParamConverter("group", options={"id" = "group_id"})
 * @ParamConverter("project", options={"id" = "project_id"})
 */
class PermissionsController extends Controller
{
    /**
     * @Route("", name="project.permissions.update.form")
     * @Method({"GET"})
     */
    public function showUpdateFormAction(Group $group, Project $project, Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            $this->createAccessDeniedException();
        }

        $form = $this->createPermissionsForm($group, $project);


        return $this->render(
            ':group/project/permissions:form_update.html.twig',
            [
                'form' => $form->createView(),
                'group' => $group,
                'project' => $project,
            ]
        );
    }

    /**
     * @Route("", name="project.permissions.update")
     * @Method({"PUT"})
     */
    public function updateAction(Group $group, Project $project, Request $request)
    {
        $form = $this->createPermissionsForm($group, $project);

        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            throw new BadRequestHttpException();
        }

        /** @var Project $project */
        $project = $form->getData();

        $em = $this->getDoctrine()->getManager();
        $dispatcher = $this->get('event_dispatcher');

        $em->persist($project);
        $em->flush();

        $dispatcher->dispatch(AppEvents::PROJECT_UPDATE_COMPLETED);

        return $this->redirectToRoute('project.list', ['group_id' => $group->getId()]
        );
    }

    /**
     * @param Group   $group
     * @param Project $project
     *
     * @return Form
     */
    private function createPermissionsForm(Group $group, Project $project)
    {
        $currentAccount = $this->getDoctrine()
            ->getRepository('AppBundle:Account')
            ->findOneByUserAndGroup($this->getUser(), $group);

        return $this->createForm(
            ProjectPermissionsType::class,
            $project,
            [
                'action' => $this->generateUrl(
                    'project.permissions.update',
                    [
                        'group_id' => $group->getId(),
                        'project_id' => $project->getId(),
                    ]
                ),
                'method' => 'PUT',
                'current_account' => $currentAccount,
            ]
        );
    }
}
