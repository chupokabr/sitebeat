<?php

namespace AppBundle\Controller\Group\Project;

use AppBundle\Entity\Group;
use AppBundle\Entity\Project;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/groups/{group_id}/projects/{project_id}/settings", requirements={"group_id": "\d+", "project_id": "\d+"})
 * @ParamConverter("group", options={"id" = "group_id"})
 * @ParamConverter("project", options={"id" = "project_id"})
 */
class ToolController extends Controller
{
    /**
     * @Route("", name="project.tool.list")
     * @Method({"GET"})
     */
    public function listAction(Group $group, Project $project)
    {
        $toolAliases = $this->get('sitebeat.scanner.rule_processor')->getDefinedToolAliases();

        return $this->redirectToRoute(
            'project.tool.update',
            [
                'group_id' => $group->getId(),
                'project_id' => $project->getId(),
                'tool' => $toolAliases[0],
            ]
        );
    }

    /**
     * @Route("/{tool}", name="project.tool.update", requirements={"tool": "\w+"})
     * @Method({"GET", "PUT"})
     */
    public function showAction(Group $group, Project $project, string $tool)
    {
        $controllerName = $this->verifyControllerDefinition($tool);

        return $this->forward(
            $controllerName.':updateAction',
            [
                'group_id' => $group->getId(),
                'project_id' => $project->getId(),
            ]
        );
    }

    /**
     * @param string $tool
     *
     * @return string
     */
    protected function verifyControllerDefinition(string $tool): string
    {
        $controllerName = sprintf('sitebeat.controller.tool.%s', $tool);

        if (!$this->has($controllerName) || !($this->get($controllerName) instanceof AbstractToolController)) {
            return $this->createNotFoundException();
        }

        return $controllerName;
    }
}
