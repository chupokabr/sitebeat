<?php

namespace AppBundle\Controller\Group\Project;

use AppBundle\Entity\Group;
use AppBundle\Entity\Project;
use AppBundle\Model\DataTransfer\AbstractToolSettingsData;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sitebeat\ScannerBundle\Entity\Rule;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AbstractToolController
 *
 * @package AppBundle\Controller\Group\Project
 */
abstract class AbstractToolController extends Controller
{
    /**
     * Example: 'domain_access'
     *
     * @return string
     */
    abstract protected static function getToolAlias(): string;

    /**
     * Because of using real Doctrine entities in forms is a real pain.
     * @see http://ocramius.github.io/doctrine-best-practices/#/57
     *
     * @param Rule $rule
     *
     * @return AbstractToolSettingsData
     */
    abstract protected function createFormDTO(Rule $rule);

    /**
     * @return string
     */
    abstract protected static function getFormTypeClassname(): string;

    /**
     * @return string
     */
    abstract protected static function getFormUpdateTemplateName(): string;

    /**
     * Update the Rule entity with submitted data from form DTO.
     *
     * @param FormInterface $form
     * @param Rule          $rule
     *
     * @return bool
     */
    abstract protected function handleFormData(FormInterface $form, Rule $rule): bool;

    /**
     * @Method({"GET", "PUT"})
     */
    public function updateAction(Group $group, Project $project, Request $request)
    {
        $rule = $this->getRuleByProject($project);

        $form = $this->createSettingsForm($this->getUpdateRuleUrl($group, $project), $rule);

        if ($request->isMethod('GET')) {
            return $this->renderSettingsResponse($group, $project, $form);
        }

        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->renderSettingsResponse($group, $project, $form);
        }

        $isDataHandled = $this->handleFormData($form, $rule);

        if ($isDataHandled) {
            $this->addFlash('success', 'Настройки теста обновлены.');
        } else {
            $this->addFlash('danger', 'Настройки теста не обновлены.');
        }

        return $this->redirect($this->getUpdateRuleUrl($group, $project));
    }

    /**
     * @param Project $project
     *
     * @return Rule
     */
    protected function getRuleByProject(Project $project)
    {
        $rule = $this->get('sitebeat.repository.rule')
            ->findOneByProjectAndTool($project, static::getToolAlias());

        if (null === $rule) {
            $rule = new Rule($project, static::getToolAlias(), 900);
        }

        return $rule;
    }

    /**
     * @param Group         $group
     * @param Project       $project
     * @param FormInterface $form
     *
     * @return Response
     */
    protected function renderSettingsResponse(Group $group, Project $project, FormInterface $form): Response
    {
        return $this->render(
            static::getFormUpdateTemplateName(),
            [
                'form' => $form->createView(),
                'project' => $project,
                'group' => $group,
                'tool_aliases' => $this->get('sitebeat.scanner.rule_processor')->getDefinedToolAliases(),
            ]
        );
    }

    /**
     * @param string $actionUrl
     * @param Rule$rule
     *
     * @return \Symfony\Component\Form\Form
     */
    private function createSettingsForm(string $actionUrl, Rule $rule)
    {
        return $this->createForm(
            static::getFormTypeClassname(),
            $this->createFormDTO($rule),
            [
                'action' => $actionUrl,
                'method' => 'PUT',
            ]
        );
    }

    /**
     * @param Group   $group
     * @param Project $project
     *
     * @return string
     */
    private function getUpdateRuleUrl(Group $group, Project $project): string
    {
        return $this->generateUrl(
            'project.tool.update',
            [
                'project_id' => $project->getId(),
                'group_id' => $group->getId(),
                'tool' => static::getToolAlias(),
            ]
        );
    }
}
