<?php

namespace AppBundle\Controller\Group;

use AppBundle\Entity\Group;
use AppBundle\Form\Type\ReportFilterType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sitebeat\ScannerBundle\Entity\Report;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/groups/{group_id}/reports", requirements={"group_id": "\d+"})
 * @ParamConverter("group", options={"id" = "group_id"})
 */
class ReportController extends Controller
{

    /**
     * todo: refactor the whole controller
     *
     * @see https://habrahabr.ru/post/319346/
     *
     * @Route("", name="group.report.list", methods={"GET"})
     */
    public function listAction(Group $group, Request $request)
    {
        $toolNames = $this->get('sitebeat.scanner.rule_processor')->getDefinedToolAliases();

        $reportRepository = $this->getDoctrine()->getRepository('SitebeatScannerBundle:Report');

        $form = $this->createFilterForm($group, $toolNames);

        if ($request->query->has($form->getName())) {
            $form->submit($request->query->get($form->getName()));

            $filterBuilder = $reportRepository->createQueryBuilder('report');
            $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($form, $filterBuilder);

            $filterBuilder->join('report.rule', 'rule')->join('rule.project', 'project')->orderBy(
                'report.createdAt',
                'DESC'
            );

            $query = $filterBuilder->getQuery();

            $form = $this->createFilterForm($group, $toolNames);
        } else {
            $query = $reportRepository->createQueryBuilder('report')
                ->join('report.rule', 'rule')
                ->join('rule.project', 'project')
                ->orderBy('report.createdAt', 'DESC')
                ->getQuery();
        }

        /** @var Report[] $reports */
        $reports = $query->getResult();

        return $this->render(
            ':group/report:list.html.twig',
            [
                'form' => $form->createView(),
                'reports' => $reports,
                'group' => $group,
            ]
        );
    }

    /**
     * @Route("/{id}", name="group.report.show")
     * @Method({"GET"})
     */
    public function showAction(Group $group, Report $report)
    {
        $controllerName = $this->verifyControllerDefinition($report->getToolAlias());

        $em = $this->getDoctrine()->getManager();
        $account = $em->getRepository('AppBundle:Account')->findOneByUserAndGroup($this->getUser(), $group);

        if ($account == $report->getAssignee()) {
            $report->markAsViewed();

            $em->persist($report);
            $em->flush();
        }

        return $this->forward($controllerName.':showAction', ['report_id' => $report->getId()]);
    }

    /**
     * @param string $tool
     *
     * @return string
     */
    protected function verifyControllerDefinition(string $tool): string
    {
        $controllerName = sprintf('sitebeat.controller.report.%s', $tool);

        if (!$this->has($controllerName) || !($this->get($controllerName) instanceof ReportControllerInterface)) {
            return $this->createNotFoundException();
        }

        return $controllerName;
    }

    /**
     * @param Group $group
     * @param       $toolAliases
     *
     * @return \Symfony\Component\Form\Form
     */
    private function createFilterForm(Group $group, $toolAliases)
    {
        return $this->createForm(
            ReportFilterType::class,
            null,
            [
                'group' => $group,
                'tool_aliases' => $toolAliases,
            ]
        );
    }
}
