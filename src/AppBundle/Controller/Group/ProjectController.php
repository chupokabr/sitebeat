<?php

namespace AppBundle\Controller\Group;

use AppBundle\AppEvents;
use AppBundle\Entity\Group;
use AppBundle\Entity\Project;
use AppBundle\Form\Type\ProjectFilterType;
use AppBundle\Form\Type\ProjectType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sitebeat\ScannerBundle\Entity\Rule;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * @Route("/groups/{group_id}/projects", requirements={"group_id": "\d+"})
 */
class ProjectController extends Controller
{
    /**
     * @Route("", name="project.list")
     * @Method({"GET"})
     * @ParamConverter("group", options={"id" = "group_id"})
     */
    public function listAction(Group $group, Request $request)
    {
        $accountRepository = $this->getDoctrine()->getRepository('AppBundle:Account');
        $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');

        $account = $accountRepository->findOneByUserAndGroup($this->getUser(), $group);
        $form = $this->createForm(ProjectFilterType::class, null, ['account' => $account]);

        if ($request->query->has($form->getName())) {
            $accountId = $request->query->get($form->getName())['account'];
            $account = $accountRepository->findOneBy(['id' => $accountId, 'group' => $group]);

            if (null === $account) {
                $this->createNotFoundException();
            }

            $form = $this->createForm(ProjectFilterType::class, null, ['account' => $account]);

            // manually bind values from the request
            $form->submit($request->query->get($form->getName()));

            // initialize a query builder
            $filterBuilder = $projectRepository->createQueryBuilder('project');

            // build the query from the given form object
            $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($form, $filterBuilder);

            $projectsQuery = $filterBuilder->getQuery();
        } else {
            $projectsQuery = $projectRepository->getBuilderByAccount($account)->getQuery();
        }

        $projects = $projectsQuery->execute();


        return $this->render(
            ':group/project:list.html.twig',
            [
                'current_account' => $account,
                'group' => $group,
                'form' => $form->createView(),
                'projects' => $projects,
            ]
        );
    }

    /**
     * @Route("/{id}", name="project.show", requirements={"id": "\d+"})
     * @Method({"GET"})
     * @ParamConverter("group", options={"id" = "group_id"})
     */
    public function showAction(Group $group, Project $project)
    {
        return $this->redirectToRoute(
            'project.tool.list',
            ['group_id' => $group->getId(), 'project_id' => $project->getId()]
        );
    }

    /**
     * @Route("/new", name="project.create.form")
     * @Method({"GET"})
     * @ParamConverter("group", options={"id" = "group_id"})
     */
    public function showCreateFormAction(Group $group)
    {
        $form = $this->createCreateForm($group);

        return $this->render(
            ':group/project:form_create.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/", name="project.create")
     * @Method({"POST"})
     * @ParamConverter("group", options={"id" = "group_id"})
     */
    public function createAction(Group $group, Request $request)
    {
        $form = $this->createCreateForm($group);

        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            throw new BadRequestHttpException();
        }

        $em = $this->get('doctrine.orm.entity_manager');
        $dispatcher = $this->get('event_dispatcher');

        $em->beginTransaction();

        try {
            /** @var Project $project */
            $project = $form->getData();
            $em->persist($project);

            //create rules for project
            $toolAliases = $this->get('sitebeat.scanner.rule_processor')->getDefinedToolAliases();
            foreach ($toolAliases as $alias) {
                $em->persist(new Rule($project, $alias, 900));
            }

            $em->flush();

            $dispatcher->dispatch(AppEvents::PROJECT_CREATE_COMPLETED);

            $em->commit();
        } catch (\Exception $exception) {
            $em->rollback();
            $dispatcher->dispatch(AppEvents::PROJECT_CREATE_ERROR);

            return $this->redirectToRoute('project.create.form', ['group_id' => $group->getId()]);
        }

        return $this->redirectToRoute(
            'project.show',
            [
                'group_id' => $group->getId(),
                'id' => $project->getId(),
            ]
        );
    }

    /**
     * @Route("/{id}/delete", name="project.delete.form", requirements={"id": "\d+"})
     * @Method({"GET"})
     * @ParamConverter("group", options={"id" = "group_id"})
     */
    public function showDeleteFormAction(Group $group, Project $project)
    {
        return $this->render(':group/project:form_delete.html.twig', ['group' => $group, 'project' => $project]);
    }

    /**
     * @Route("/{id}", name="project.delete", requirements={"id": "\d+"})
     * @Method({"DELETE"})
     */
    public function deleteAction(Project $project)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $dispatcher = $this->get('event_dispatcher');

        $entityManager->remove($project);
        $entityManager->flush();

        //перенести в слушатель
        $dispatcher->dispatch(AppEvents::PROJECT_DELETE_COMPLETED);

        return $this->redirectToRoute(
            'project.list',
            ['group_id' => $project->getGroup()->getId()]
        );
    }

    /**
     * @param Group        $group
     * @param Project|null $project
     *
     * @return \Symfony\Component\Form\Form
     */
    private function createCreateForm(Group $group, Project $project = null)
    {
        $currentAccount = $this->getDoctrine()
            ->getRepository('AppBundle:Account')
            ->findOneByUserAndGroup($this->getUser(), $group);

        return $this->createForm(
            ProjectType::class,
            $project,
            [
                'action' => $this->generateUrl('project.create', ['group_id' => $group->getId()]),
                'method' => 'POST',
                'current_account' => $currentAccount,
            ]
        );
    }
}
