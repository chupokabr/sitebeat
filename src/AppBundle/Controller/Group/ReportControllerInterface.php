<?php

namespace AppBundle\Controller\Group;

use Sitebeat\ScannerBundle\Entity\Report;

/**
 * Interface ReportControllerInterface
 *
 * @package AppBundle\Controller\Group
 */
interface ReportControllerInterface
{
    public function showAction(Report $report);
}
