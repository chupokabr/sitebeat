<?php

namespace AppBundle\Controller\Group;

use AppBundle\Entity\Group;
use Kelnik\UserBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * @Route("/groups/{group_id}/members")
 * @ParamConverter("group", options={"id" = "group_id"})
 */
class AccountController extends Controller
{
    /**
     * @Route("", name="group.account.list", methods={"GET"})
     */
    public function listAction(Group $group)
    {
        /** @var User $user */
        $user = $this->getUser();

        $currentAccount = $this->getDoctrine()->getRepository('AppBundle:Account')
            ->findOneByUserAndGroup($user, $group);

        $accounts = $this->getDoctrine()->getRepository('AppBundle:Account')->findByGroup($group);

        return $this->render(
            ':group/account:list.html.twig',
            [
                'current_account' => $currentAccount,
                'group' => $group,
                'accounts' => $accounts,
            ]
        );
    }
}
