<?php

namespace AppBundle\Controller;

use AppBundle\AppEvents;
use AppBundle\Entity\Account;
use AppBundle\Entity\Group;
use AppBundle\Form\Type\Group\CreateGroupType;
use AppBundle\Form\Type\Group\UpdateGroupType;
use AppBundle\Model\DataTransfer\GroupData;
use Kelnik\UserBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * @Route("/groups")
 */
class GroupController extends Controller
{

    /**
     * @Route("/{id}/delete", name="group.delete.form", requirements={"id": "\d+"})
     * @Method("GET")
     */
    public function showDeleteFormAction(Group $group)
    {
        return $this->render(':group:form_delete.html.twig', ['group' => $group]);
    }

    /**
     * @Route("/{id}/edit", name="group.update.form", requirements={"id": "\d+"})
     * @Method("GET")
     */
    public function showUpdateFormAction(Group $group)
    {
        $form = $this->createForm(
            UpdateGroupType::class,
            GroupData::fromGroupModel($group),
            ['action' => $this->generateUrl('group.update', ['id' => $group->getId()])]
        );

        return $this->render(':group:form_update.html.twig', ['form' => $form->createView(), 'group' => $group]);
    }

    /**
     * @Route("/{id}", name="group.show", requirements={"id": "\d+"})
     * @Method("GET")
     */
    public function showAction(Group $group)
    {
        return $this->forward('AppBundle:Group/Project:list', ['group_id' => $group->getId()]);
    }

    /**
     * @Route("", name="group.list")
     * @Method("GET")
     */
    public function listAction()
    {
        /** @var User $user */
        $user = $this->getUser();

        $accounts = $this->getDoctrine()->getRepository('AppBundle:Account')->findByUser($user);

        return $this->render(':group:list.html.twig', ['accounts' => $accounts]);
    }

    /**
     * @Route("/{id}", name="group.update", requirements={"id": "\d+"})
     * @Method("PUT")
     */
    public function updateAction(Group $group, Request $request)
    {
        $form = $this->createForm(UpdateGroupType::class, GroupData::fromGroupModel($group))->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            throw new BadRequestHttpException();
        }
        $objectManager = $this->getDoctrine()->getManager();
        $dispatcher = $this->get('event_dispatcher');

        /** @var GroupData $data */
        $data = $form->getData();

        $group->renameWith($data->getName());

        $objectManager->persist($group);
        $objectManager->flush();

        $dispatcher->dispatch(AppEvents::GROUP_UPDATE_SUCCESS);

        return $this->redirectToRoute('project.list', ['group_id' => $group->getId()]);
    }

    /**
     * @Route("/new", name="group.create")
     * @Method({"GET","POST"})
     */
    public function createAction(Request $request)
    {
        $form = $this->createForm(CreateGroupType::class, new GroupData());

        if ($request->isMethod('GET')) {
            return $this->render(':group:form_create.html.twig', ['form' => $form->createView()]);
        }

        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->render(':group:form_create.html.twig', ['form' => $form->createView()]);
        }

        $dispatcher = $this->get('event_dispatcher');

        /** @var GroupData $data */
        $data = $form->getData();

        $entityManager = $this->get('doctrine.orm.entity_manager');
        $entityManager->beginTransaction();

        try {
            //create group
            $group = new Group($data->getName());
            $entityManager->persist($group);

            /** @var User $user */
            $user = $this->getUser();

            // create administrator account for group
            $account = new Account($user, $group, Account::GROUP_ADMIN);

            $entityManager->persist($account);
            $entityManager->flush();
            $entityManager->commit();
        } catch (\Exception $exception) {
            $entityManager->rollback();
            $dispatcher->dispatch(AppEvents::GROUP_CREATE_ERROR);

            return $this->redirectToRoute('group.create');
        }

        $dispatcher->dispatch(AppEvents::GROUP_CREATE_SUCCESS);

        return $this->redirectToRoute('project.list', ['group_id' => $group->getId()]);
    }

    /**
     * @Route("/{id}", name="group.delete")
     * @Method("DELETE")
     */
    public function deleteAction(Group $group)
    {
        $em = $this->getDoctrine()->getManager();
        $dispatcher = $this->get('event_dispatcher');

        $em->remove($group);
        $em->flush();

        $dispatcher->dispatch(AppEvents::GROUP_DELETE_SUCCESS);

        return $this->redirectToRoute('homepage');
    }
}
