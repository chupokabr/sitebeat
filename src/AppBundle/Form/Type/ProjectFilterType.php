<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\Account;
use AppBundle\Entity\AccountRepository;
use AppBundle\Entity\Project;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\EntityFilterType;
use Lexik\Bundle\FormFilterBundle\Filter\Query\QueryInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProjectFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Account $account */
        $account = $options['account'];

        $builder->add(
            'account',
            EntityFilterType::class,
            [
                'class' => Account::class,
                'data' => $account,
                'mapped' => false,
                'required' => true,
                'query_builder' => function (AccountRepository $repository) use ($account) {
                    return $repository->getBuilderByGroup($account->getGroup());
                },
                'apply_filter' => function (QueryInterface $filterQuery, $field, $values) {
                    /** @var Account $account */
                    $account = $values['value'];

                    if (null === $account || $account->isRole(Account::GROUP_ADMIN)) {
                        return null;
                    }

                    $expression = ':account MEMBER OF project.subscribers';

                    return $filterQuery->createCondition($expression, ['account' => $account]);
                },
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'csrf_protection' => false,
                'method' => 'GET',
                'data_class' => Project::class,
                'empty_data' => null,
            ]
        )
            ->setRequired('account')
            ->addAllowedTypes('account', Account::class);
    }
}
