<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\AccountRepository;
use AppBundle\Entity\Account;
use AppBundle\Entity\Project;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ProjectType
 *
 * @package AppBundle\Form\Type
 */
class ProjectType extends AbstractType implements DataMapperInterface
{
    /**
     * @var AccountRepository
     */
    private $accountRepository;

    /**
     * ProjectType constructor.
     *
     * @param AccountRepository $accountRepository
     */
    public function __construct(AccountRepository $accountRepository)
    {
        $this->accountRepository = $accountRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Account $currentAccount */
        $currentAccount = $options['current_account'];

        $groupAccounts = $this->accountRepository->findByGroup($currentAccount->getGroup());

        $builder->setDataMapper($this);


        $assignee = $builder->getData() ? $builder->getData()->getAssignee() : $currentAccount;

        $assigneeChoices = array_filter(
            $groupAccounts,
            function (Account $account) {
                return $account->getRole() !== Account::GROUP_GUEST;
            }
        );

        $editorsChoices = array_filter(
            $assigneeChoices,
            function (Account $account) use ($assignee) {
                return $account != $assignee;
            }
        );

        $viewersChoices = array_diff($groupAccounts, $assigneeChoices);

        $builder
            ->add('uri', UrlType::class)
            ->add(
                'assignee',
                EntityType::class,
                [
                    'class' => Account::class,
                    'data' => $assignee,
                    'choices' => $assigneeChoices,
                    'required' => true,
                    'placeholder' => false,
                ]
            )->add(
                'editors',
                EntityType::class,
                [
                    'class' => Account::class,
                    'multiple' => true,
                    'expanded' => true,
                    'choices' => $editorsChoices,
                ]
            )->add(
                'viewers',
                EntityType::class,
                [
                    'class' => Account::class,
                    'multiple' => true,
                    'expanded' => true,
                    'choices' => $viewersChoices,
                ]
            );
    }


    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Project::class,
                'empty_data' => function (FormInterface $form) {
                    return new Project(
                        $form->get('uri')->getData(),
                        $form->get('assignee')->getData()
                    );
                },
            ]
        )
            ->setRequired(['current_account'])
            ->setAllowedTypes('current_account', Account::class);
    }

    /**
     * @param Project         $project
     * @param FormInterface[] $forms
     */
    public function mapDataToForms($project, $forms)
    {
        if (null === $project) {
            return;
        }

        $forms = iterator_to_array($forms);

        $accounts = ['editors' => [], 'viewers' => []];
        $subscribers = $project->getSubscribers();

        foreach ($subscribers as $subscriber) {
            if ($subscriber->getRole() === Account::GROUP_GUEST) {
                $accounts['viewers'][] = $subscriber;
            } else {
                $accounts['editors'][] = $subscriber;
            }
        }

        $forms['editors']->setData($accounts['editors']);
        $forms['viewers']->setData($accounts['viewers']);
    }

    /**
     * @param FormInterface[] $forms
     * @param Project         $project
     */
    public function mapFormsToData($forms, &$project)
    {
        $forms = iterator_to_array($forms);

        $assignee = $forms['assignee']->getData();
        $editors = $forms['editors']->getData();
        $viewers = $forms['viewers']->getData();

        $project->getSubscribers()->clear();

        foreach ($editors as $editor) {
            $project->addSubscriber($editor);
        }

        foreach ($viewers as $viewer) {
            $project->addSubscriber($viewer);
        }

        $project->setAssignee($assignee);
    }
}
