<?php

namespace AppBundle\Form\Type\Group;

use AppBundle\Form\Type\GroupType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UpdateGroupType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['method' => 'PUT']);
    }

    public function getParent()
    {
        return GroupType::class;
    }
}
