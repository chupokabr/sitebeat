<?php

namespace AppBundle\Form\Type\Group;

use AppBundle\Form\Type\GroupType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CreateGroupType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['method' => 'POST']);
    }

    public function getParent()
    {
        return GroupType::class;
    }
}
