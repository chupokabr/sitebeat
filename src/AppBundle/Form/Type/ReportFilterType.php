<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\Account;
use AppBundle\Entity\AccountRepository;
use AppBundle\Entity\Group;
use AppBundle\Entity\Project;
use AppBundle\Entity\ProjectRepository;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\ChoiceFilterType;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\DateTimeRangeFilterType;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\EntityFilterType;
use Lexik\Bundle\FormFilterBundle\Filter\Query\QueryInterface;
use Sitebeat\ScannerBundle\Model\Report;
use Sitebeat\ScannerBundle\Tool\AbstractTool;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ReportFilterType
 */
class ReportFilterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Group $group */
        $group = $options['group'];

        $toolAliases = $options['tool_aliases'];

        $builder
            ->add(
                'createdAt',
                DateTimeRangeFilterType::class,
                [
                    'left_datetime_options' => [
                        'widget' => 'single_text',
                        'attr' => ['class' => 'js-datepicker'],
                    ],
                    'right_datetime_options' => [
                        'widget' => 'single_text',
                        'attr' => ['class' => 'js-datepicker'],
                    ],
                    'mapped' => false,
                ]
            )->add(
                'project',
                EntityFilterType::class,
                [
                    'data_class' => Project::class,
                    'class' => Project::class,
                    'choice_label' => 'uri',
                    'query_builder' => function (ProjectRepository $repository) use ($group) {
                        return $repository->createQueryBuilder('project')
                            ->leftJoin('project.assignee', 'assignee', 'WITH', 'assignee.group = :group')
                            ->setParameter('group', $group);
                    },
                    'mapped' => false,
                    'apply_filter' => function (QueryInterface $filterQuery, $field, $values) {
                        if (empty($values['value'])) {
                            return null;
                        }

                        $expression = 'rule.project = :project';

                        return $filterQuery->createCondition($expression, ['project' => $values['value']]);
                    },
                ]
            )
            ->add(
                'assignee',
                EntityFilterType::class,
                [
                    'data_class' => Account::class,
                    'class' => Account::class,
                    'query_builder' => function (AccountRepository $repository) use ($group) {
                        return $repository->createQueryBuilder('account')
                            ->where('account.group = :group')
                            ->setParameter('group', $group);
                    },
                    'mapped' => false,
                    'apply_filter' => function (QueryInterface $filterQuery, $field, $values) {
                        if (empty($values['value'])) {
                            return null;
                        }

                        $expression = 'project.assignee = :assignee';

                        return $filterQuery->createCondition($expression, ['assignee' => (string) $values['value']]);
                    },
                ]
            )->add(
                'tool',
                ChoiceFilterType::class,
                [
                    'choices' => array_combine($toolAliases, $toolAliases),
                    'apply_filter' => function (QueryInterface $filterQuery, $field, $values) {
                        if (empty($values['value'])) {
                            return null;
                        }

                        $expression = 'rule.toolAlias = :tool_alias';

                        $tool = $values['value'];

                        return $filterQuery->createCondition($expression, ['tool_alias' => $tool]);
                    },
                ]
            )->add(
                'statusLevel',
                ChoiceFilterType::class,
                [
                    'choices' => array_flip(Report::STATUS_CODES),
                ]
            );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Report::class,
                'method' => 'GET',
                'error_bubbling' => true,
                'csrf_protection' => false,
                'empty_data' => null,
            ]
        )
            ->setRequired(['group', 'tool_aliases'])
            ->setAllowedTypes('group', Group::class);
    }
}
