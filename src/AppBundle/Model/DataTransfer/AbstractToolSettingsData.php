<?php

namespace AppBundle\Model\DataTransfer;

use Sitebeat\ScannerBundle\Entity\Rule;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Interface ToolSettingsDataInterface
 *
 * @package AppBundle\Model\DataTransfer
 */
abstract class AbstractToolSettingsData
{
    /**
     * @var int
     *
     * @Assert\NotBlank()
     * @Assert\Range(min="1")
     */
    protected $runInterval = 900;
    /**
     * @var bool
     */
    protected $enabled = false;

    /**
     * @param Rule $rule
     *
     * @return static
     */
    public static function fromRuleModel(Rule $rule)
    {
        $data = new static();
        $data->setRunInterval($rule->getRunInterval());
        $data->setEnabled($rule->isEnabled());

        return $data;
    }

    /**
     * @return int
     */
    public function getRunInterval()
    {
        return $this->runInterval;
    }

    /**
     * @param int $runInterval
     */
    public function setRunInterval(int $runInterval)
    {
        $this->runInterval = $runInterval;
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     */
    public function setEnabled(bool $enabled)
    {
        $this->enabled = $enabled;
    }
}
