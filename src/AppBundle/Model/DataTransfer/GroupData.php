<?php

namespace AppBundle\Model\DataTransfer;

use AppBundle\Model\Group;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class GroupData
 *
 * @package AppBundle\Model\DataTransfer
 */
class GroupData
{
    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(min="4", max="40")
     */
    protected $name = '';

    /**
     * @param Group $group
     *
     * @return static
     */
    public static function fromGroupModel(Group $group)
    {
        $data = new static();
        $data->setName($group->getName());

        return $data;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }
}
