<?php

namespace AppBundle\Model;

/**
 * Class Group.
 */
class Group
{
    /**
     * @var string
     */
    protected $name;

    /**
     * Group constructor.
     *
     * @param string $name
     */
    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function renameWith($name)
    {
        $this->name = $name;
    }
}
