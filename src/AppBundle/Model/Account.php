<?php

namespace AppBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Kelnik\UserBundle\Model\UserInterface;

/**
 * Class Account represents user in a context of group.
 */
class Account
{
    /**
     * @var string
     */
    const GROUP_ADMIN = 'group_admin';
    /**
     * @var string
     */
    const GROUP_MANAGER = 'group_manager';
    /**
     * @var string
     */
    const GROUP_GUEST = 'group_guest';

    /**
     *
     */
    const GROUP_ROLES = [
        self::GROUP_ADMIN,
        self::GROUP_MANAGER,
        self::GROUP_GUEST,
    ];

    /**
     * @var UserInterface
     */
    protected $user;
    /**
     * @var Group
     */
    protected $group;
    /**
     * @var string Role in a context of group. Can be admin, manager or guest
     */
    protected $role;
    /**
     * @var Project[]
     */
    protected $projects;

    /**
     * Account constructor.
     *
     * @param UserInterface $user
     * @param Group         $group
     * @param string        $role
     */
    public function __construct(UserInterface $user, Group $group, string $role)
    {
        $this->projects = new ArrayCollection();
        $this->user = $user;
        $this->group = $group;
        $this->role = $role;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getUser()->getFirstName().' '.$this->getUser()->getLastName();
    }

    /**
     * @return UserInterface
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return Group
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @param string $role
     *
     * @return bool
     */
    public function isRole(string $role): bool
    {
        return $this->getRole() === $role;
    }

    /**
     * @return string
     */
    public function getRole(): string
    {
        return $this->role;
    }

    /**
     * @param string $role
     */
    public function setRole(string $role)
    {
        $this->role = $role;
    }

    /**
     * @param Project $project
     */
    public function addProject(Project $project)
    {
        $this->projects->add($project);
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->user->getEmailCanonical();
    }
}
