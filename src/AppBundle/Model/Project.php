<?php

namespace AppBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Sitebeat\ScannerBundle\Model\Report;
use Sitebeat\ScannerBundle\Model\Rule;

/**
 * The class represents site in a context of a project group.
 */
class Project
{
    /**
     * @var string
     */
    protected $uri;
    /**
     * @var Account
     */
    protected $assignee;
    /**
     * @var Account[]
     */
    protected $subscribers;
    /**
     * @var Rule[]
     */
    protected $rules;

    /**
     * Project constructor.
     *
     * @param string  $uri
     * @param Account $assignee
     */
    public function __construct(string $uri, Account $assignee)
    {
        $this->rules = new ArrayCollection();
        $this->subscribers = new ArrayCollection();

        $this->uri = $uri;
        $this->setAssignee($assignee);
    }

    /**
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * @param string $uri
     */
    public function setUri(string $uri)
    {
        $this->uri = $uri;
    }

    /**
     * @return Group
     */
    public function getGroup()
    {
        return $this->assignee->getGroup();
    }

    /**
     * @return Account
     */
    public function getAssignee()
    {
        return $this->assignee;
    }

    /**
     * @param Account $assignee
     */
    public function setAssignee(Account $assignee)
    {
        $this->subscribers->removeElement($this->assignee);
        $this->addSubscriber($assignee);
        $this->assignee = $assignee;
    }

    /**
     * @param Rule $rule
     */
    public function addRule(Rule $rule)
    {
        $this->rules[$rule->getToolAlias()] = $rule;
    }

    /**
     * @return ArrayCollection|Report[]
     */
    public function getLatestReports()
    {
        $reports = new ArrayCollection();

        foreach ($this->rules as $rule) {
            if ($rule->hasReports()) {
                $reports[] = $rule->getLastReport();
            }
        }

        return $reports;
    }

    /**
     * Return all reports with status code worse than "success".
     *
     * @return ArrayCollection|Report[]
     */
    public function getLatestNotSuccessfulReports()
    {
        $reports = $this->getLatestReports();

        return $reports->filter(
            function (Report $report) {
                return Report::SUCCESS < $report->getStatusLevel();
            }
        );
    }

    /**
     * @return bool
     */
    public function hasNotSuccessfulReports(): bool
    {
        $reports = $this->getLatestReports();

        return !$reports->forAll(
            function ($key, Report $report) {
                return Report::SUCCESS === $report->getStatusLevel();
            }
        );
    }

    /**
     * @return ArrayCollection|Account[]
     */
    public function getSubscribers()
    {
        return $this->subscribers;
    }

    /**
     * @param Account $account
     */
    public function addSubscriber(Account $account)
    {
        if (!$this->subscribers->contains($account)) {
            $account->addProject($this);
            $this->subscribers->add($account);
        }
    }
}
