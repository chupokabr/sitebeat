var $collectionHolder = $('.form-collection');
$collectionHolder.data('index', $collectionHolder.children().length);

$(document).on('click', '.collection-remove', function (event) {
    var itemId = $(this).data('item');
    event.preventDefault();
    $('#' + itemId).remove();
});

$(document).on('click', '.collection-add', function (event) {
    event.preventDefault();
    var $dataPrototype = $collectionHolder.data('prototype');
    var index = $collectionHolder.data('index');

    $dataPrototype = $dataPrototype.replace(/__name__/ig, index);
    $collectionHolder.append($dataPrototype);

    $collectionHolder.data('index', index + 1);
});

$(document).on('click', '.collection-clear', function (event) {
    event.preventDefault();
    $collectionHolder.html('');


    $collectionHolder.data('index', 0);
});