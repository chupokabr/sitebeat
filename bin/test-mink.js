const Browser = require('zombie');
Browser.localhost('tveda.ru', 80);

Browser.extend(function (browser) {
    // intercept ajax requests
    browser.on('xhr', function (event, url) {
        console.log(url);
    });
});

var browser = new Browser;
browser.visit('/', function () {
});