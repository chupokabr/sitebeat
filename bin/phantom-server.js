// initialise various variables
var page = require('webpage').create(),
    system = require('system'),
    address;

// how long should we wait for the page to load before we exit
// in ms
var WAIT_TIME = 5000;

// if the page hasn't loaded after this long, something is probably wrong.
// in ms
var MAX_EXECUTION_TIME = 15000;

// output error messages
var DEBUG = false;

const GOOGLE = 'google';
const YANDEX = 'yandex';
const GOOGLE_OLD = 'google_old';

// address is the url passed
address = system.args[1];

/**
 * A container of tracker URLs requested on page
 */
var trackerRequestUrlCollection = {
    google: [],
    yandex: []
};

/**
 * A collection of RegExps to extract query values from request uri and callbacks for creating a tracker object.
 * Callback output must look like: {"serviceName": string, "trackingId": string[, "hitType": string]}
 */
var regexpCollection = [
    [new RegExp('(?:(?:www|ssl)?\\.google-analytics\\.com|stats\\.g\\.doubleclick\\.net)(?:.*?)collect\\?(.*)', 'i'), GOOGLE],
    [new RegExp('(?:(?:www|ssl)?\\.google-analytics\\.com|stats\\.g\\.doubleclick\\.net)(?:.*?)utm\\.gif\\?(.*)', 'i'), GOOGLE],
    [new RegExp('mc\\.yandex\\.ru\\/watch\\/([\\d]+)\\?', 'i'), YANDEX]
];

// create a function that is called every time a resource is requested
// http://phantomjs.org/api/webpage/handler/on-resource-requested.html
page.onResourceRequested = function (resource) {
    for (var i = 0; i < regexpCollection.length; i++) {
        var regexp = regexpCollection[i][0];

        if (regexp.test(resource.url)) {
            trackerRequestUrlCollection[regexpCollection[i][1]].push(resource.url);
        }
    }
};

// if debug is true, log errors, else ignore them
page.onError = function (msg, trace) {
    if (DEBUG) {
        console.log('ERROR: ' + msg);
        console.log(trace)
    }
};

// make a note of any errors so we can print them out
page.onResourceError = function (resourceError) {
    page.reason = resourceError.errorString;
    page.reason_url = resourceError.url;
};

// now all we have to do is open the page, wait WAIT_TIME ms and exit
try {
    page.open(address, function (status) {
        if (status !== 'success') {
            console.log("FAILED: to load " + system.args[1]);
            console.log(page.reason_url);
            console.log(page.reason);
            phantom.exit();
        } else {
            if (address != page.url) {
                console.log('Redirected: ' + page.url)
            }
            setTimeout(function () {
                phantom.exit();
            }, WAIT_TIME);

            console.log((JSON.stringify(trackerRequestUrlCollection, null, ' ')))
        }
    });
} catch (e) {
    console.log(JSON.stringify(e));
} finally {
    // if we are still running after MAX_EXECUTION_TIME ms exit
    setTimeout(function () {
        console.log("FAILED: Max execution time " + Math.round(MAX_EXECUTION_TIME) + " seconds exceeded");
        phantom.exit(1);
    }, MAX_EXECUTION_TIME);
}
