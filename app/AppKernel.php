<?php

use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\HttpKernel\Kernel;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = [
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),

            new Doctrine\Bundle\DoctrineCacheBundle\DoctrineCacheBundle(),
            new FOS\UserBundle\FOSUserBundle(),
            new \Lexik\Bundle\FormFilterBundle\LexikFormFilterBundle(),
            new Cron\CronBundle\CronCronBundle(),

            new \Kelnik\UserBundle\KelnikUserBundle(),
            new Sitebeat\ScannerBundle\SitebeatScannerBundle(),
            new Sitebeat\NotifierBundle\SitebeatNotifierBundle(),
            new AppBundle\AppBundle(),

            new Sitebeat\Tools\PageAccessBundle\SitebeatToolsPageAccessBundle(),
            new Sitebeat\Tools\DomainAccessBundle\SitebeatToolsDomainAccessBundle(),
            new Sitebeat\Tools\IframeBundle\SitebeatToolsIframeBundle(),
            new Sitebeat\Tools\NoindexBundle\SitebeatToolsNoindexBundle(),
            new Sitebeat\Tools\RobotsTxtBundle\SitebeatToolsRobotsTxtBundle(),
        ];

        if (in_array($this->getEnvironment(), ['dev', 'test'], true)) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
            $bundles[] = new Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle();
        }

        return $bundles;
    }

    public function getRootDir()
    {
        return __DIR__;
    }

    public function getCacheDir()
    {
        return dirname(__DIR__).'/var/cache/'.$this->getEnvironment();
    }

    public function getLogDir()
    {
        return dirname(__DIR__).'/var/logs';
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load($this->getRootDir().'/config/config_'.$this->getEnvironment().'.yml');
    }
}
